<?php
require_once __DIR__ . '/resizeJpg.function.php';
require_once __DIR__ . '/recursiveProcessDirectory.function.php';
function myPreCommitHook(array $fileNames, string $currentWorkingDir): void
{
    $function = 'resizeJpg';

    foreach ($fileNames as $fileName) {
        if (!str_starts_with($fileName, '?? ')) {
            continue;
        }

        $fileName = str_replace('?? ', '', $fileName);
        $filePath = "{$currentWorkingDir}/{$fileName}";

        if (is_dir($filePath)) {
            recursiveProcessDirectory($filePath, $function);
            continue;
        }
        $function($filePath);
    }
}


