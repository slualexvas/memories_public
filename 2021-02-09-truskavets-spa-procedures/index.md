 Трускавец: стоун-массаж и другие СПА-процедуры 
================================================

В последующие дни мы с Кариночкой опробовали разные процедуры:

Стоун массаж
------------

Это массаж с помощью горячих камней. Сами камни выглядят примерно вот так:

![](0.jpg)  

Суть идеи в том, чтобы прогреть нужные участки тела, направив в них тепло из этих камней.

По уверениям массажистки, стоун-массаж — это очень эффективная в плане прогревания техника; она намного эффективнее, чем, например, сауна.

Сначала массажистка взяла в свои руки две крупные каменюги (самые крупные из тех, что на фото) и начала меня массировать этими каменюгами (перед этим смазав массажным маслом). Это было очень приятно, ведь каменюги были тёплые, почти горячие. А когда массажистка прошлась каменюгами по пояснице, меня чуть не вырубило.

Потом массажистка закинула крупные каменюги в кастрюлю с тёплой водой (чтобы они снова набрали тепло), а сама положила мне на размятую спину полотенце, на это полотенце начала выкладывать кучу мелких камешков, а когда вся спина была в камешках, то накрыла это всё ещё одним полотенцем. Эта штука должна была прогреть мне спину. Правда, по ощущениям, не очень прогрела.

А пока моя спина прогревалась, массажистка прошлась нагревшимися большими каменюгами по рукам, ногам и шее. Так что, в целом, прикольная штука, и многообещающая.

**Небольшое физическое отступление:**

По уверением массажистки, идеальные камни для стоун-массажа — это вулканическая лава, застывшая в море. Они намного эффективнее речных камней, а речные камни, в свою очередь, намного эффективнее любого другого материала (метал, пластик, глина, грелки с водой). Насколько это правда, я судить не берусь.

Душ Шарко
---------

Душ Шарко — это когда тебя «расстреливают» струями воды.

![Я стоял под лампочкой](1.jpg "Я стоял под лампочкой") Я стоял под лампочкой 

![Валерия расстреливала меня вот этими двумя шлангами](2.jpg "Валерия расстреливала меня вот этими двумя шлангами") Валерия расстреливала меня вот этими двумя шлангами 

![Вот они, поближе](3.jpg "Вот они, поближе") Вот они, поближе 

Обёртывания
-----------

Отель предлагал несколько видов обёртываний. Кариночка сходила на шоколадное обёртывание и на медовое. Соответственно в один день она вкусно пахла шоколадом, а во второй — мёдом.

В третий день Кариночка на обёртывания на пошла, потому что в комплекте с обёртываниями идёт пилинг, и она сказала, что её кожа этого не вынесет :)

Вместо обёртываний Кариночка захотела «ванну красоты».

«Ванна красоты»
---------------

Отель предлагал несколько (где-то 6-8) разновидностей ванн. Это были ванны с хвоей, с минералами, с лекарственными травами... Кариночка колебалась между двумя ваннами:

- «Ванна красоты»  
- «Ванна Клеопатры»

Когда мы спросили у отельного косметолога, то нам рассказали, что «ванна красоты» — это когда в тёплую воду добавляется около литра молока и чуть меньше мёда; а «ванна Клеопатры» — это порошковое молоко + порошковый мёд + хитровыделанная французская косметика Mary Cohr. Подумав, мы решили отдать предпочтение натуральному и выбрали «ванну красоты».

Выглядела она вот так:

![](4.jpg)  

Диана: реабилитация отельного массажа
-------------------------------------

Как я уже раньше писал, мне делал массаж всего тела какой-то парень. И мне не понравилось.

Но массаж мне нужен, поэтому я рискнул ещё раз и записался на массаж спины и шейно-воротниковой зоны. На этот раз его делала девушка по имени Диана. Она была реально великолепна, и массаж был так же хорош, как и в Киеве в «Клинике современной ревматологии». Поэтому я к Диане сходил ещё раз и привёл туда невесту.

Вывод: в отеле есть не только плохой массаж, но и хороший :)

Цены
----

Массаж всего тела (1 час) — 420 грн  
Фитобочка с массажем — 300 грн  
Стоун-массаж (1 час) — 550 грн  
Массаж головы (15 минут) — 150 грн  
Обёртывание — 300 грн  
Прессотерапия (45 минут) — 300 грн  
Душ Шарко (10 минут) — 190 грн  
Массаж спины и воротниковой зоны (30 минут) — 310 грн