# Krupa Art Gallery

- Место: на главной площади Вроцлава
- Google Maps: https://maps.app.goo.gl/b33aBPxhDiTAtvAT9
- Цена билетов: 35 злотых с человека

## Выставка "Tropy"

### Картина "rzeka życia":

![01-rzeka-zycia.jpg](images/01-rzeka-zycia.jpg)

### Картина "20 materiałów". 

Это не совсем картина, а скорее аппликация из тканей:

![02-20-materialow.jpg](images/02-20-materialow.jpg)

### Картина "Po trzydziestu pięciu latach"

К этой картине было описание:

> Edward Dwurnik, Pola Dwurnik - "Po trzydziestu pięciu latach"
>
> To pierwszy obraz, który tata namalował ze mną w Worpswede. Miałam pięć lat i za nic w świecie nie chciałam iść do niemieckiego przedszkola. Całe dnie siedziałam w pracowni taty, nic więc dziwnego, że zaprosił mnie do współpracy.  
Tytuł obrazu nawiązuje do różnicy wieku między nami – 35 lat.
> 
> **Farbami akrylowymi namalowałam dolną część obrazu, tak wysoko, jak tylko mogłam dosięgnąć.**  
Można powiedzieć, że ten obraz opowiada o grupie ludzi kiedyś i dziś. Kiedyś byli dziećmi i malowali słońce, a teraz walczą o wolność w opresyjnym ciemnym kraju.
> 
> — *Pola Dwurnik*


![03-po-35-latach.jpg](images/03-po-35-latach.jpg)

### Картина "Она предпочитает закат солнца, а не стаю козлов"

![04-she-prefers-a-sunset.jpg](images/04-she-prefers-a-sunset.jpg)

Предупреждаю сразу: из этой фотографии ещё недостаточно понятно, что это вообще такое. Поэтому советую посетить эту страницу и насладиться дополнительными фотками и перечнем материалов: https://piktogram.org/pl/sklep/90722-agata-ingarden-she-prefers-a-sunset-to-a-flock-of-goats

### Картина "весенние вишни"

![05-spring-cherries.jpg](images/05-spring-cherries.jpg)

### Экспонат "скульптура из камней, которую надо носить на теле"

Экспонат выглядит как ремешки, к которым прикреплены камни. Рядом же с экспонатом есть фотка "инструкции по применению":

![06-stones-on-body.jpg](images/06-stones-on-body.jpg)

P.S. Как и всегда, здесь фотки менее чем половины экспонатов.

## Интерактивчик 1: смайлики

- Рядом с каждым экспонатом есть магнитные доски
- В начале выставки есть коробка со смайликами
- Замысел в том, что если у тебя некий экспонат вызвал эмоцию, то ты можешь взять из коробки смайлинк и наклеить на магнитную доску рядом с этим экспонатом.

Вот так выглядит коробка:

![07-box-with-smiles.jpg](images/07-box-with-smiles.jpg)

А вот так выглядит магнитная доска со смайликами:

![08-magnet-board-with-smiles.jpg](images/08-magnet-board-with-smiles.jpg)

## Интерактивчик 2: камни

При входе на выставку есть несколько камней. Под камнями есть надпись: "можешь взять один камень с собой и носить его в руке/кармане/где-то ещё, пока смотришь выставку. После окончания осмотра не забудь вернуть камень на место". Выглядит это вот так:

![09-stones.jpg](images/09-stones.jpg)

## Интерактивчик 3: нарисуй свою картину

История такая:
- Автор взял шаблон "лист бумаги разделён на несколько прямоугольников" и нарисовал на нём свою картину
- Я картину автора показывать не буду, потому что картина отвратная: там кишки и прочая гадость
- Потом автор предлагает посетителям выставки нарисовать свою картину, используя тот же шаблон "лист бумаги разделён на несколько прямоугольников"

Вот постановка задачи:

![10-rectangles-task.jpg](images/10-rectangles-task.jpg)

А вот - различные решения, сделанные посетителями выставки:

![11-rectangles-solution.jpg](images/11-rectangles-solution.jpg)

![12-rectangles-solution.jpg](images/12-rectangles-solution.jpg)

![13-rectangles-solution.jpg](images/13-rectangles-solution.jpg)

![14-rectabgles-solution.jpg](images/14-rectabgles-solution.jpg)

![15-rectangles-solution.jpg](images/15-rectangles-solution.jpg)

![16-rectangles-solution.jpg](images/16-rectangles-solution.jpg)

В общем, мой вывод: посетители выставки (т.е. непрофессиональные художники) нарисовали более приятно, чем профессиональный художник.

И ещё один вывод: цена завышена. Злотых до 10 - было бы оправдано. А вот 35 злотых - это уже перебор.