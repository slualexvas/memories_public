 Трускавец, день 2: фитобочка 
==============================

Фитобочка — это такая спа-процедура, на которой тебя обдают паром от кипятящихся растений. Пар имеет температуру 40-50 градусов, и эту температуру можно регулировать. Выглядит фитобочка вот так:

![](0.jpg)  

![](1.jpg)  

Сотрудница спа-центра застелила сидение одноразовой простынкой, потом обмотала мне шею махровым полотенце («чтобы пар не выходил» — объяснила она), посадила меня в бочку и закрыла дверцы сбоку и сверху.

Само устройство очень уж похоже на колодки (античное и средневековое орудие фиксирования и пытки, что-то вроде деревянных кандалов).

![](2.jpg)  

Поэтому я, пока в этой бочке сидел, первые 7 минут думал о том, не пришла ли идея об этой бочке от инквизиции. Потом решил, что всё же не пришла, потому что количество пара в бочке нельзя быстро регулировать: т.е. нельзя быстро убавить пар, чтобы спросить у пытаемого, где базы повстанцев :)

В целом же фитобочка, как мне и говорили, оказалась «как влажная баня, только лучше». В том плане, что она пропаривает тело, но мозг остаётся в относительно комфортной среде.

После 15 минут фитобочки меня посадили за стол и налили травяной чай:

![](3.jpg)  

Где-то на 10 минут меня оставили отдышаться с этим чаем. А после этого положили на массажный стол и сделали лёгенький массаж с ароматическим маслом:

![](4.jpg)  

Весь процесс занял около часа. Мне понравилось :)