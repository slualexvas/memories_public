# Идёт ли мне борода?

![with-beard.jpg](with-beard.jpg)

UPD: идентичной фотографии без бороды не нашлось, поэтому добавляю максимально похожую:

![without-beard.jpg](without-beard.jpg)
