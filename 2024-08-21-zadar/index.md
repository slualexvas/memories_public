# Прогулка по городу "Задар" в Хорватии

Во первых строках письма хочу отметить, что, согласно Википедии, в городе Задар проживает 71 тысяча человек. То есть этот город меньше, чем Валбжих.

Но зато, в отличие от Валбжиха, в Задаре есть аэропорт.

## Пляж

Почти по всему берегу тянутся пляжи. Пляжи выглядят примерно так:

![01-beach.jpg](01-beach.jpg)

На подавляющем большинстве пляжей людей сильно побольше, чем на этом. Вот пример:

![02-beach.jpg](02-beach.jpg)

## Дорога над пляжем

Над пляжами идёт дорога со свежепостеленным асфальтом: 

![03-road-near-beach.jpg](03-road-near-beach.jpg)

Предназначение дороги - променад, т.е. лёгкие ненапряжные прогулки пешком или на велосипеде/самокате.

## Маленький порт

Пройдя по вышеуказанной дороге минут 20, я добрался до пристани:

![04-little-port.jpg](04-little-port.jpg)

Это маленькая пристань; есть ещё и большая. Но мне у этой пристани понравилось то, что рядом с табличкой улицы есть ещё и картинка:

![05-little-port-table-with-picture.jpg](05-little-port-table-with-picture.jpg)

Считаю, что такое надо сделать на все улицы во всех городах.

## Старый город

Пройдя за порт, я увидел вход в старый город. Вход оформлен вот такой красиовй аркой:

![06-oldtown-arc.jpg](06-oldtown-arc.jpg)

Сам старый город меня не впечатлил. Я его даже сфоткать забыл. Зато я сфоткал народное творчество на стенах в подворотне:

![07-graffiti.jpg](07-graffiti.jpg)

Я решил, что я уже отошёл от дома достаточно далеко, и пора поворачивать к дому. Поэтому я повернул направо, прошёл старый город, и вышел к большому порту. 

Прежде чем перейти к большому порту, я также обращу ваше внимание на то, как оформлен край парка - высокой (от 3 до 10 метров) каменной стеной:

![08-park-wall.jpg](08-park-wall.jpg)

## Большой порт

Большой порт - намного больше, чем маленький. Площадь воды тоже намного больше, и кораблей намного больше, и здания на берегах новые:

![09-big-port.jpg](09-big-port.jpg)

## Густозаселённые улицы города

После порта я пошёл густозаселёнными улицами города. Выглядели они примерно вот так:

![11-dense-popularity-streets.jpg](11-dense-popularity-streets.jpg)

На этих самых густозаселённых улицах я вышел сначала к супермаркету Interspar, а потом и к автовокзалу, к которому мы уже приехали.

В супермаркет я зашёл и посмотрел. Поставил себе на заметку два вида товаров, которые в Хорватии дорогие, но их бОльшее разнообразие, чем в Польше:
1. Мясные изделия, вроде колбас "суджукица" и "чайная", а также прошутто (цены за килограмм - от 60 до 100 злотых)
2. Твёрдые сыры (цены за килограмм - от 40 до 80 злотых)

## Малозаселённые улицы города

После густозаселённых улиц города я перешёл в малозаселённые. Они застроены одно- и двухэтажными виллами; в одной из таких вилл мы арендовали аппартаменты по цене "110 евро (450 злотых) за ночь за две комнаты с кухней и санузлом".

Вот так выглядят малозаселённые улицы:

![12-sparse-popularity-streets.jpg](12-sparse-popularity-streets.jpg)

На прощание покажу вам школу, на этих же малозаселённых улицах:

![13-school.jpg](13-school.jpg)

P.S. "вход" и "выход" переводятся на хорватский язык как "ulaz" и "izlaz".
