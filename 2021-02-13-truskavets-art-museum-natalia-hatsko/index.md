 Наталия Хацко: графика на стекле 
==================================

Как я уже писал, Трускавецкий художественный музей посвящён Михаилу Биласу. Но они выделили одну небольшую комнатку на выставку работ молодой художницы Наталии Хацко.

Наталия рисует на стекле. Вот как это выглядит:

![](0.jpg)  

![](1.jpg)  

![](2.jpg)  

![](3.jpg)  

![](4.jpg)  

![Вот эта — моя любимая](5.jpg "Вот эта — моя любимая") Вот эта — моя любимая 

![](6.jpg)  

![](7.jpg)  

![](8.jpg)  

![](9.jpg)  

![](10.jpg)  

![](11.jpg)  

![](13.jpg)  

![](14.jpg)  

![](15.jpg)  

![](16.jpg)  

![](17.jpg)  

![](18.jpg)  

![](19.jpg)  

![](20.jpg)  

![](21.jpg)  

![](22.jpg)  

А вот и сама Наталия:

![](23.jpg)