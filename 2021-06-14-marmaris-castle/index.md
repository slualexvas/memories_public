 Неудавшаяся поездка в Мармарисский замок-музей 
================================================

Жена очень хотела попасть в Мармарисский замок-музей. Поэтому в последний день мы сели на такси (40 лир от отеля до замка) и минут за 15 были на краю «старого города». Внутри старого города машины не ездят, поэтому мы распрощались с такси и пошли пешком.

Старый город
------------

Старый город представляет собой симпатичного вида трущобы:

![](0.jpg)  

Кстати, за домами видно краешек замковой стены.

Мы шли по этим трущобам и вовсю рассматривали домики:

![](1.jpg)  

Тем временем замок был всё виднее:

![](2.jpg)  

Возле домиков было очень много зелени. Турки молодцы: понаставляли себе кучу разных бадеек, вёдер и тому подобного, и выращивали в них всякие цветочки:

![](3.jpg)  

Один раз даже было видно море:

![](4.jpg)  

Вот здесь была явно какая-то тюрьма:

![](5.jpg)  

Мы шли вокруг замка, потихонечку поднимаясь вверх:

![](6.jpg)  

![](7.jpg)  

![](8.jpg)  

И вот, когда мы, наконец, дошли до входа в музей — оказалось, что музей закрыт. На вопрос «А когда откроется?» охранники ответили «завтра».

Мы огорчились и пошли из трущоб.

Торговые улочки
---------------

Через какое-то время мы вышли в город. Город представлял собой небольшие улочки с магазинчиками по краям:

![](9.jpg)  

![](10.jpg)  

Вымощены улочки плиткой, среди которой попадаются интересные мозаики:

![](11.jpg)  

На улочках много памятников, но практически нигде нету подписей:

![](12.jpg)  

![](13.jpg)  

![](14.jpg)  

![](15.jpg)  

Обычные улицы
-------------

Выйдя из торговых улочек, мы увидели большую и красивую площадь с фонтаном. Это одна из Мармарисских достопримечательность — поющие фонтаны. Мы же её благополучно потеряли, потому что пришли днём:

![](16.jpg)  

![](17.jpg)  

Перейдя площадь и углубившись в улицы, мы увидели турецкую школу:

![](18.jpg)  

Естественно, она была имени Ататюрка:

![](19.jpg)  

Рядом со школой росла белая шелковица:

![](20.jpg)  

Также рядышком был садик. Тоже имени Ататюрка:

![](21.jpg)  

Чуть дальше начались жилые районы. В жилом районе меня заинтересовал один из балконов:

![](22.jpg)  

На балконе сушится бельё (это ожидаемо), а ещё стоят банки с каким-то вареньем (это неожиданно). То ли они бражку так готовят... Хотя банки вроде закатаны:

![](23.jpg)  

Также мы видели заправку, и даже сфоткали цены на топливо:

![](24.jpg)  

Напоминаю, что одна турецкая лира равна приблизительно три грн или 0.12 USD. Таким образом, один литр 95-го бензина стОит 24 грн или 95 центов.

Видели мы и канал, но он был напрочь пересохший:

![](25.jpg)  

Очень трогательно называется магазин штор:

![](26.jpg)  

Мы прошли вдоль канала по направлению к набережной. В результате канал увлажнился, а мы увидели ещё один красивый памятник:

![](27.jpg)  

Турецкие названия продолжают меня радовать. Например, магазин офисных принадлежностей «Атас» (я привык воспринимать это слово как «бежим», «тревога», «шухер» и тд).

![](28.jpg)  

Ещё дальше канал был уже с водой, а по краям его были каменные рыбки. И, конечно же, ещё один памятник:

![](29.jpg)  

Мы свернули на улочку, идущую к набережной. Купили по мороженке:

![](30.jpg)  

![](31.jpg)  

А попутно сфоткали ещё один памятник:

![](32.jpg)  

![](33.jpg)  

И вышли к ещё одному каналу. Он был более мелким, но зато в нём тусовалось куча рыбок. Мы им кинули вафельку от мороженого, а какой-то другой парень пошвырял им крупные кусочки булки. Рыбки на эти кусочки азартно накинулись:

https://liubopytnyi.livejournal.com/video/album/378/?mode=view&id=11514#rprecord=eyJpZCI6MTc0Mjc0MCwicmVjb3JkSWQiOjE3NDI3NDB9

Набережная
----------

Вот так, идя по ходу канала, мы вышли на набережную:

![](34.jpg)  

В самом начале набережной мне очень понравилась вывеска фаст-фуда:

![](35.jpg)  

Дальше был красивый памятник:

![](36.jpg)  

Даже мусорник сделан не просто так, а красиво:

![](37.jpg)  

Памятников там много и разных:

![](38.jpg)  

Пройдя чуть дальше, мы увидели полицейских на мотоциклах. Они знаками показали, что мы должны надеть маски. Мы надели маски, а потом решили немного отойти с набережной и маски сдвинуть на подбородок. И не зря, потому что мы увидели изумительной красоты пагоду:

![](39.jpg)  

Рядом с пагодой был бассейн, в котором выращивалось много растений прям в горшках:

![](40.jpg)  

Позже мы вернулись на набережную:

![](41.jpg)  

Мы прошли по набережной аж до торгового центра, в котором жена собиралась пошопиться с горя, что её не пустили в музей; но торговый центр тоже был закрыт. Поэтому мы нашли таксиста и уехали в отель пить чай.

Поездка в такси и турецкие деньги
---------------------------------

С таксистом ещё тоже был нюанс, связанный с тем, что у меня не было налички. Поэтому пришлось снять немного денег в банкомате:

![](42.jpg)  

Таксист запросил 25 лир, а банкомат отдавал только двадцатки:

![](43.jpg)  

Поэтому таксист получил 40 лир и отдал 15 лир сдачи:

![](44.jpg)  

На десятке заметна математическая формула. Оказывается, это инвариант Арфа — инвариант квадратичной формы по модулю «2», заданной на целочисленной решётке, снабжённой билинейной кососимметричной формой.

Про самого Арфа можно почитать здесь: [https://ru.wikipedia.org/wiki/Арф,\_Джахит](https://ru.wikipedia.org/wiki/Арф,_Джахит)

Пятёрка тоже отметилась:

![](45.jpg)  

На пятёрке изображён Айдын Сайылы — [https://ru.wikipedia.org/wiki/Сайылы,\_Айдын](https://ru.wikipedia.org/wiki/Сайылы,_Айдын).

Это турецкий историк и популяризатор науки. Поэтому на банкноту также впихнули модель атома и молекулу ДНК.

Пока мы это всё рассматривали, таксист привёз нас в отель. Мы поблагодарили его и пошли в лобби-бар пить чай и отдыхать после насыщенной прогулки.

P.S.
----

По дороге мы спросили у таксиста, почему всё закрыто. Оказалось, что в Турции сейчас действует карантин выходного дня.