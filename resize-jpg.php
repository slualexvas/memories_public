<?php
require_once __DIR__ . '/resizeJpg.function.php';
require_once __DIR__ . '/recursiveProcessDirectory.function.php';

$path = $argv[1];
if (str_ends_with($path, '.jpg')) {
    resizeJpg($path);
} else {
    recursiveProcessDirectory($path, 'resizeJpg');
}
