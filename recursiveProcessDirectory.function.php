<?php

function recursiveProcessDirectory(string $dir, $function)
{
    $items = scandir($dir);
    foreach ($items as $item) {
        if (in_array($item, ['.', '..'])) {
            continue;
        }

        $itemPath = "{$dir}/{$item}";
        $function($itemPath);

        if (is_dir($itemPath)) {
            recursiveProcessDirectory($itemPath, $function);
        }
    }
}