 Велосипедные покатушки в Ичмелер 
==================================

Как я уже писал, наш отель «Labranda mares Marmaris» находится между двумя городами — Мармарисом и Ичмелером. До Мармариса 5 км, до Ичмелера — 3,5. Это хорошо видно на карте:

![](0.jpg)  

В Мармарисе мы уже побывали, когда ездили в магазин «Silvio Leather». А вот в Ичмелере мы ещё не были. Кроме того, где-то в 7 минутах ходьбы от отеля мы обнаружили мужика, который сдавал велосипеды в аренду.

Поэтому мы взяли у мужика велосипеды и решили скататься в Ичмелер. Велосипеды взяли на два часа и заплатили по 5 долларов за каждого — т.е. всего 10 долларов:

![](1.jpg)  

И вот мы потихонечку поехали. Слева от нас было красивенное море:

![](2.jpg)  

Справа от нас сначала было два или три отеля. Потом отели закончились, и дальше был просто горный склон:

![](3.jpg)  

Через 10-15 минут таких покатушек мы доехали до Ичмелера. Город встретил нас пляжем:

![](4.jpg)  

Возле пляжа стояло несколько отелей. Мы проехали мимо пляжа, пока не увидели выкрашенную в голубой цвет велосипедную дорожку, уходящую в город. Вот по этой дорожке мы и поехали.

По обе стороны от дорожки было много красивых зданий:

![](5.jpg)  

Подавляющее большинство ориентировано на туристов: рестораны, кафе, магазинчики...

![](6.jpg)  

Меня удивил странный памятник. На нём написано, что это памятник губке:

![](7.jpg)  

На заднем плане видно велосипедную дорожку, выкрашенную в голубой цвет.

Мы выехали на небольшую площадь, на которой были фонтаны в форме дельфинов:

![](8.jpg)  

Ещё там была какая-то странная колоннада и шахматная доска. Что символизирует сия композиция, я не понял:

![](9.jpg)  

Ну и для завершённости культурного шока приезжих — куча статуэток, начиная от грудастой тётки и заканчивая какими-то солдатами:

![](10.jpg)  

![](11.jpg)  

![](12.jpg)  

![](13.jpg)  

Также на площади есть автоматизированная аренда велосипедов. «Автоматизированная» означает, что там нету человека, зато есть автоматическая парковка, которая удерживает велосипеды в замках, и при оплате их высвобождает:

![](14.jpg)  

Как и чем оплачивается аренда, я не понял. Я даже не понял, сколько аренда стОит. Единственная информация — вот:

![](15.jpg)  

Как бы там ни было, мы поехали дальше. Улыбнулись, увидев кофейню Илли, знакомую по Киеву:

![](16.jpg)  

Внезапно велодорожка отказалась вести нас дальше. Она поворачивала и налево, и направо; а вот прямо не шла. Сначала мы повернули налево, и достигли знаменитого Ичмелерского канала. Вот он на карте Ичмелера:

![](17.jpg)  

Сам канал выглядит не очень полноводным:

![](18.jpg)  

Зато в нём активно тусят индоутки:

![](19.jpg)  

Ещё в канале активно тусит рыба, причём довольно крупная. Мы видели стаю примерно на 200 особей, и каждая особь весом в 30-50 граммов — то есть их вполне можно было бы ловить на удочку.

На следующей фотке, если приглядеться, можно разглядеть стаю:

![](20.jpg)  

Что интересно, уткам на этих рыб пофигу. Одна из уток проплыла прямо через стаю, так рыбки лениво расступались (точнее, расплывались) перед ней где-то на 10 см. Утка даже и не подумала на них поохотиться.

Моя несравненная, непревзойдённая и горячо любимая жена сняла этих рыбок на видео:



На канале велодорожка, повернувшая налево, закончилась. Мы вздохнули, вернулись на развилку и поехали по велодорожке направо. К сожалению, справа тоже не было ничего особо интересного, кроме памятника турецкому национальному герою по имени Мустафа Кемаль и прозвищу Ататюрк:

![](21.jpg)  

![](22.jpg)  

Возле Ататюрка велодорожка тоже заканчивалась; таким образом, с неё по-любому надо было съезжать. Жена захотела пофоткаться на фоне гор; горы находились слева от велодорожки, т.е. за каналом; поэтому мы вернулись к каналу и съехали с велодорожки.

Город очень быстро закончился (буквально за пару кварталов) и начался частный сектор:

![](23.jpg)  

Деревья с серыми листьями, которые видно в левой части фото — это оливки. Вот они более крупным планом:

![](24.jpg)  

Ещё мы видели курей (но не сфоткали из уважения к их частной жизни) и лимоны:

![](25.jpg)  

В частном секторе мы снова увидели канал, но он сильно зарос камышом:

![](26.jpg)  

По другую сторону от канала были шикарно подстриженные деревья:

![](27.jpg)  

Пройдя немного по течению канала, мы обнаружили, что он пересох:

![](28.jpg)  

А на следующей фотке вы видите сразу две интересности:

— Деревянный мостик

— Инжир

![](29.jpg)  

Идя дальше по течению канала, мы вышли из частного сектора и вернулись в туристическую часть города. Но не в центр. В центре много магазинов и ресторанов, а в этой части было много отелей:

![](30.jpg)  

![](31.jpg)  

Во дворе одного из отелей видно дерево с плодами. А что это за плоды, мы так и не поняли:

![](32.jpg)  

А тем временем в канале уже появилась вода:

![](33.jpg)  

Идя дальше по течению, мы вышли на то место, где снимали рыбок, а потом и ещё дальше, где канал стал глубже:

![](34.jpg)  

И вот, пару кварталов спустя, мы вышли к морю. У моря канал уже красивый:

![](35.jpg)  

![](36.jpg)  

Уже прошло больше часа из нашей двухчасовой аренды велосипедов, поэтому мы поехали обратно. По дороге увидели ещё один канал, очень коротенький:

![](37.jpg)  

Выехали из Ичмелера, и снова ехали по дороге возле моря. Обратили внимание на красивенные фонарики в зарослях:

![](38.jpg)  

![](39.jpg)  

![](40.jpg)  

От фонариков было совсем близко до мужика, у которого мы арендовали велосипеды. Мы ему их отдали и расстались, довольные друг другом. Вся поездка в Ичмелер заняла полтора часа.