# Łowisko, Smażalnia " Nad Potokiem"

Ссылка на карту Google: https://maps.app.goo.gl/fsa3jzDfRgRMhzaF9

Адрес: Grzędy 65A, 58-379

Речь идёт о ресторане, который находится над ручьём, а также над прудом, сделанным на этом самом ручье. В этом пруду разводят форель; также эту самую форель жарят, и угощают ею гостей в ресторане.

## Интерьер

Внутри ресторана находится около десяти столиков на 4 человека каждый. Столики находятся возле окон; в окна можно любоваться прудом:

![01-table.jpg](01-table-at-window.jpg)

К сожалению, не со всех столиков можно любоваться прудом, однако все являются комфортными:

![02-another-tables.jpg](02-another-tables.jpg)

Также внутри ресторана есть чугунная печь со стеклянными дверцами. Эту печь топят дровами, и благодаря этому помещание хорошо прогревается.

![03-owen.jpg](03-owen.jpg)

## Территория снаружи ресторана

Вот видео: https://youtu.be/nMT-JYaig8c
- В начале видео видно ручей
- Потом видно территорию (качели и тд)
- В конце видео видно большой пруд

## Блюда и цены

Основное блюдо - это рыба (форель). Вес одной рыбины - около 300-400 г. Цены:
- На жареную рыбу: 13 злотых за 100 г
- На рыбу, приготовленную на парУ: 15 злотых за 100 г
- На копчёную рыбу (горячее копчение): 8 злотых за 100 г

Также есть дополнительные блюда:
- Картошка фри (9 злотых)
- Салат из овощей (9 злотых)
- Чай, пепси (около 10-15 злотых)

![04-fried-fish.jpg](04-fried-fish.jpg)

## Итоги
- Чек на одного человека = 50-80 злотых
- Моя оценка ресторана = твёрдая "пятёрка"
- Важное примечание: платить карточкой - нельзя. Только либо наличка, либо BLIK.