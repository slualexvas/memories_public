<?php
require_once __DIR__ . '/vendor/autoload.php';

getArticleFromLivejournal(112992, 'gift-from-wife-to-men-day', '2022-02-23');

function getArticleFromLivejournal(int $postId, string $dirName, string $date): void
{
    $client = Symfony\Component\HttpClient\HttpClient::create([
        "verify_peer" => false,
        "verify_host" => false,
    ]);

    $browser = new \Symfony\Component\BrowserKit\HttpBrowser($client);

    // Авторизация
    $browser->request(
        method: 'POST',
        uri: 'https://www.livejournal.com/__api/',
        content: '[{"jsonrpc":"2.0","method":"user.login","params":{"user":"liubopytnyi","password":"RznKJ82b","expire":"never","auth_token":"sessionless:1681743600:/__api/::edd50b3e3268f77c34dfe9e3e445f499fba4064a"},"id":7}]'
    );

    // Авторизовались, теперь получаем текст поста
    $browser->request('GET', "https://liubopytnyi.livejournal.com/{$postId}.html");
    $htmlContent = $browser->getResponse()->getContent();

    $dom = new DOMDocument();
    @$dom->loadHTML($htmlContent);

//    $date = ($dom->getElementsByTagName('time'))[0]->textContent;
//    $date = date('Y-m-d', strtotime($date));
    $dirName = "{$date}-{$dirName}";

    echo "created folder {$dirName}\n";

    $dirPath = __DIR__ . '/' . $dirName;

    mkdir($dirPath);
//    file_put_contents($dirPath . '/index.html', $htmlContent);

    $articleTitle = ($dom->getElementsByTagName('title'))[0]->textContent;
    str_replace(': liubopytnyi — LiveJournal', '', $articleTitle);

    $articleText = ($dom->getElementsByTagName('article'))[1]->C14N();

    $articleText = mb_substr($articleText, mb_strpos($articleText, '<h1'));

    $domArticle = new DOMDocument();
    @$domArticle->loadHTML($articleText);

    /*
 <figure class="aentry-post__figure aentry-post__figure--media"><iframe src="https://l.lj-toys.com/?auth_token=sessionless%3A1680638400%3Aembedcontent%3A86080041%2632%26%26%26youtube%26uZelDren8Ak%3A6b86577924560958d263d3e4b30ccef2a124a29d&amp;source=youtube&amp;vid=uZelDren8Ak&amp;moduleid=32&amp;preview=&amp;journalid=86080041&amp;noads=" width="740" height="416" frameborder="0" class="lj_embedcontent" allowfullscreen name="embed_86080041_32"></iframe></figure>

 * */
    $youtubeVideos = $domArticle->getElementsByTagName('iframe');
    foreach ($youtubeVideos as $video) {
        $src = $video->getAttribute('src');

        if (!str_contains($src, 'source=youtube')) {
            continue;
        }

        if (!preg_match('/vid=([^&]+)/', $src, $matches)) {
            continue;
        }

        $vid = $matches[1];
        $youtubeUrl = "https://www.youtube.com/watch?v={$vid}";

        $articleText = str_replace($video->C14N(), "\n\n" . $youtubeUrl . "\n\n", $articleText);
    }

    // Получаем ссылки на изображения из HTML-кода
    $img_tags = $domArticle->getElementsByTagName('img');
    $img_urls = [];
    foreach ($img_tags as $tag) {
        $img_urls[] = $tag->getAttribute('src');
    }

    // Загружаем и сохраняем изображения на сервере
    foreach ($img_urls as $i => $url) {
        echo "process image {$url}\n";

        $browser->request('GET', $url);

        $img_data = $browser->getResponse()->getContent();
        $extension = pathinfo($url, PATHINFO_EXTENSION);

        $filename = "{$i}.{$extension}";
        $filePath = $dirPath . '/' . $filename;
        file_put_contents($filePath, $img_data);

        if ($extension != 'jpg') {
            $convertedFilename = "{$i}.jpg";
            $convertedFilePath = $dirPath . '/' . $convertedFilename;

            system("convert {$filePath} {$convertedFilePath}");
            unlink($filePath);

//            $filePath = $convertedFilePath;
            $filename = $convertedFilename;
        }

        $articleText = str_replace($url, $filename, $articleText);
    }

    $converter = new \League\HTMLToMarkdown\HtmlConverter();
    $converter->getConfig()->setOption('strip_tags', true);
    $markdown = $converter->convert($articleText);

    // Убираем Livejournal-кат (кнопку "Развернуть полный текст")
    $markdown = str_replace('<a name="cutid1"></a>', '', $markdown);

    // Убираем лишние пробелы в начале строк
    while (true) {
        $newMarkdown = str_replace("\n ", "\n", $markdown);
        if ($newMarkdown === $markdown) {
            break;
        } else {
            $markdown = $newMarkdown;
        }
    }

    $markdownPath = $dirPath . '/index.md';
    file_put_contents($markdownPath, $markdown);

    echo "Completed! Check file {$markdownPath}\n";
}
