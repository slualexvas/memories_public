<?php

function resizeJpg(string $path)
{
    if (str_ends_with($path, '-orig-size.jpg')) {
        return;
    }

    $needSizeInKilobytes = 50;
    if (filesize($path) <= $needSizeInKilobytes * 1024 * 1.05) {
        return;
    }

    $cmd = "mogrify -resize 640x -quality 70% " . escapeshellarg($path);
    system($cmd);
}