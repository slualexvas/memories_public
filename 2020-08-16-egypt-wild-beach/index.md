 Египет-2020: прогулка по дикому пляжу 
=======================================

### Предыстория

В первый день, когда мы приехали, был сильный ветер. На пляже был красный флаг, что означало "купаться запрещено". С горя мы пошли гулять по окрестностям пляжа.

### Прогулка

Окрестности пляжа были дикими местами, которые ещё не успели застроиться отелями:

![](0.jpg)  

Мы услышали странный цокот. Далеко не сразу удалось заметить, что цокот издают мелкие крабики. Эти крабики носят на себе ракушки и шарятся по берегу:

[https://vc.videos.livejournal.com/index/player?record\_id=1484252&amp;player=new&amp;player\_template\_id=3869](https://vc.videos.livejournal.com/index/player?record_id=1484252&player=new&player_template_id=3869) 

[https://vc.videos.livejournal.com/index/player?record\_id=1484254&amp;player=new&amp;player\_template\_id=3869](https://vc.videos.livejournal.com/index/player?record_id=1484254&player=new&player_template_id=3869) 

Ещё на берегу было подозрительно много пустых двустворчатых ракушек. Мне кажется, есть какие-то предприимчивые египтяне, которые ищут жемчуг, а пустые ракушки выбрасывают на берег, радуя этим всех окрестных крабиков. А иногда ещё и туристок:

[https://vc.videos.livejournal.com/index/player?record\_id=1484255&amp;player=new&amp;player\_template\_id=3869](https://vc.videos.livejournal.com/index/player?record_id=1484255&player=new&player_template_id=3869) 

Хотя попадаются и не только двустворчатые ракушки:

![](1.jpg)  

![](2.jpg)  

![](3.jpg)  

Напоследок же мне удалось заснять какое-то животное, подозрительно похожее на кальмара:

[https://vc.videos.livejournal.com/index/player?record\_id=1484257&amp;player=new&amp;player\_template\_id=3869](https://vc.videos.livejournal.com/index/player?record_id=1484257&player=new&player_template_id=3869) 









[Египет](https://liubopytnyi.livejournal.com/tag/%D0%95%D0%B3%D0%B8%D0%BF%D0%B5%D1%82)