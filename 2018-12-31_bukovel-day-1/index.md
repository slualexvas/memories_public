### Поездка и поселение

2018-12-22, в 19:00 отправился поезд из Киева в Ивано-Франковск (около 200 грн за билет). В поезде была наша молодая компания. В компании ехала Юля (опытная лыжница и наш организатор) с родственниками, трое Саш (я -- один из них), а также Оля, Алла, Кетевани, Иван, и прочие хорошие люди. Мы вырвались на три дня в курорт Буковель, покататься на лыжах.

Утром следующего дня, 23 декабря, в 06:00 утра, поезд приехал в Ивано-Франковск. Мы вышли из поезда и пошли к зданию вокзала. Там уже стоял микроавтобус, с водителем которого Юля заранее договорилась. Мы погрузились в микроавтобус и поехали в Буковель.

Дороги были, мягко говоря, не фонтан. Первая причина "нефонтанности" -- это горы, снег, ухабы. Вторая причина -- это пробки, вызванные тем, что все рванули в Буковель на эти выходные (не одни ж мы такие умные!). Спасибо нашему водителю: он и крутился, и вертелся, и маневрировал, и протискивался... Благодаря этому мы ни в одной пробке не застряли надолго и радостно прибыли в Буковель, в частную гостиницу "У Наташи". Водителю за такой подвиг заплатили по 150 грн с человека.

В гостинице в 8 часов утра два номера были свободны (а нам было нужно 5 или 6 номеров). Остальных номеров нужно было ждать до времени заселения (12 или 14 часов, не помню точно). Поэтому мы покидали вещи в эти два номера и пошли кормиться.

По дороге на кормёжку сделано было вот такое фото зимнего Буковеля *(обратите внимание, как ласково встающее солнышко освещает вершину горы):*

![](01.jpg)

Сама гостиница мне понравилась, хотя и три звезды. И постель, и душ, и общий комфорт были шикарные. Цены для Буковеля в сезон были очень по-божески: 550 грн за ночь с человека. Вот здесь есть видео-обзор гостиницы: <https://www.youtube.com/watch?v=HrRLjrvPCds>.

Кормились мы в ресторане "У Наташи", который был при этой же гостинице и принадлежал ей же. Кормили очень прилично (например, в день приезда были бутерброды с маслом, сыром и колбасой + яичница из трёх яиц с беконом + узвар). Стоила кормёжка по 230 грн за сутки (сутки = завтрак и обед; ужина они не предлагали).

![](14.jpg)

### Осмотр Буковеля и запись на занятие

После кормёжки Юля повела нас осматривать Буковель. Мы увидели супермаркет "Aston", вареничную "BORZO vareniki", баню, и прочие заведения Буковеля. Через время мы подошли к подъёмникам.

Буковель активно развивается и старается строить побольше лыжной инфраструктуры, чтобы обслужить побольше туристов и настричь с них побольше денежек. Но на эти коротенькие выходные понаехала такая орава народу, что даже вся понастроенная инфраструктура не успевала их обслужить, и всюду стояли очереди. Вот так примерно это выглядело:

![](04.jpg)

![](05.jpg)

Юля показала нам, где можно записаться на обучение лыжному катанию, а сама пошла кататься. Мы в здоровенной очереди стояли минут 40. В течение этих 40-ка минут между мной (бывшим преподавателем) и ещё одним Сашей (действующим преподавателем) разгорелся спор методического характера "какое занятие брать: индивидуальное или групповое". Саша аргументировал тем, что на инд. занятии будет инд. подход, и мы лучше усвоим знания. Я же топил за то, что за 4 часа объяснения группе мы лучше усвоим, чем за два часа объяснения лично нам, тк будем наблюдать за чужими ошибками. (Индивидуальное занятие длится 2 часа и стоит 1300 грн + 700 грн за каждого следующего участника; Групповое занятие длится 4 часа и стоит 1000 грн с каждого участника).

Как оказалось, наши методические аргументы ничего не стоили в столкновении с суровой действительностью: все индивидуальные занятия были уже забронированы на сегодня и на завтра. На групповые занятия сегодня мы не могли попасть, тк не успевали на обед (групповое занятие = 4 часа, помните?). Поэтому мы записались на групповое занятие на 10 часов утра на завтра и пошли домой.

Конечно, был открытым вопрос "есть ещё 3 часа до обеда (16:00); куда мы их будем девать?". Но этот вопрос очень легко решил третий Саша, завёдший нас в небольшой ларёчек продегустировать карпатское пиво, исключительно с исследовательскими намерениями.

Потом был обед, а после обеда мы, как подуставшие от ночи в поезде, разбрелись по номерам. Формальная цель была "поселиться", а фактическая -- "полежать".

Меня поселили с одним из Саш (который препод). Мы разговорились, с обсуждения студенток разговор плавно перешёл на ряды Фурье, и вот уже через 15 минут мы увлечённо копались в формулах из его статьи.

![](formula.png)Потом подошёл Юлин папа, который вообще-то гуманитарий, но человек очень и очень умный, и тем для разговоров нам хватило аж до ужина.

В ресторане "У Наташи" ужина почему-то не давали (я уже писал выше: 230 грн за завтрак + обед. Без ужина). Поэтому мы спионерили из холла электрочайник, каждый достал еду, припасенную для поезда (лично у меня это были ириски + козинаки) и устроили посиделки с чаем. Где-то к 10ти часам вечера народ разбрёлся спать, а к нам пришёл третий Саша. Он оказался разрядником по шахматам, и время до полуночи прошло в сражениях на шахматной доске и распиваниях коньяка вторым и третьим Сашами.

Когда мы уже решили закругляться, я перевернул доску и начал собирать фигурки. Внезапно второй Саша увидел, что на обратной стороне доски находится поле для нард и загорелся: "Народ, а вы умеете в нарды играть? О, а давайте научу!". Тут я понял, что спать мне ещё долго не светит... :)

![](nardi.jpg)

