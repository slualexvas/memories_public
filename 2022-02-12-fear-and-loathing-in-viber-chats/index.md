 Страх и ненависть в чатиках Viber 
===================================

На этой неделе я был в гостях у родителей жены. Тесть и тёща живут в дачном кооперативе, а у этого кооператива есть вайбер-чатик.

В один солнечный зимний день я, жена, тёща и племянник вышли покататься на санках. Тогда мы ещё не знали, чем закончится наша прогулка.

Мы вышли на берег пруда, посадили на санки племянника и столкнули его вниз. Племянник (40 кг) съехал, и ему понравилось. Понравилось также и жене с тёщей (по 60 кг каждая). А вот мне с моими 90 кг понравилось чуть меньше, потому что я съехал дальше всех и пропахал санками примерно три метра тростника.

Я поднялся на ноги и вышел на берег. И уже на берегу обнаружилось, что с моей руки капает кровь — порезал о тростник. Я залепил руку снегом, но через пару минут обнаружилось, что кровь не останавливается. Мы подождали, пока тёща договорит с соседкой (встретили соседку, которая выводила на прогулку своих коз) и пошли домой.

По дороге домой мы на одной из улиц увидели мёртвую собаку. Собаку сфотографировали, а фотографию сбросили председателю дачного кооператива.

По дороге я ещё пару раз менял снег на руке, потому что кровь не останавливалась. Потом мы пришли домой, я промыл рану водой с мылом, потом промыл крепчайшим самогоном и заклеил пластырем. Сейчас уже порез зажил и всё хорошо. Казалось бы, ситуация исчерпана. Но мы не знали, какая драма разожглась в Viber-чатике кооператива.

Итак, наслаждайтесь:

**Председатель**: Возле 533-го участка найдена мёртвая (к сожалению) собака. Хозяин, просим вас отозваться. Если не отзовётесь, администрация похоронит собаку самостоятельно.

**Елена**: Жесть...

**Тамила**: Да когда же это закончится? Что за нелюди убивают наших собак?

**Илона** сбрасывает фотку:

![](0.jpg)  

**Илона:** Собака ЗАСТРЕЛЕНА!!! Вы что, вообще с ума сошли? РУКИ МЫЛИ ОТ КРОВИ!!!

**Наталья:** Ужас какой!

**Илона:** (сбрасывает другие фотки окровавленного снега)

**Илона:** Убитую собаку тащили по всему селу!

**Илона:** (сбрасывает фотку снега с козьими какашками)

**Ольга:** Следы коз?

**Ольга:** Может, владельцы коз что-то видели?

**Дима:** А козы целы?

**Татьяна (владелица коз):** Козы целы. А кровь — это человек поранился, когда катался на горке.

**Ольга:** Человек, который стрелял?

**Дима:** Да, именно он

**Татьяна:** Нет, который на санках катался

**Дима:** Так санки у вас во дворе стояли, возле калитки

**Ольга:** Такая хорошая собачка была, всех к себе подпускала.

**Даша+Саша Еврономер:** А вот меня не подпускала, всегда гавкала.

**Ольга:** Наверное, и убийцу к себе не подпустила, и он с санок застрелил.

<a name="cutid1-end"></a>