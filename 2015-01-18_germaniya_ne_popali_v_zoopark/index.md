Утро 18 января началось для нас с удивления: мы проснулись и увидели, что выпал снег. В той части Германии, где мы находимся, снег - это редкость. Да вы и сами это заметили: не зря ведь тут в середине зимы плющ зелёный и розовые цветы на кустах. Вот так выглядит наш дом с выпавшим снегом вокруг:

![](01.jpg)

Поскольку 18 января - это воскресенье, я решил, что надо выгулять жену и предложил ей пойти в зоопарк. Мы уже видели раньше хороший фотоотчёт о билефельдском зоопарке от [Хвойной](http://yolka-izh.livejournal.com/250694.html), и хотели лицезреть эту красоту своими глазами. Поэтому мы зашли на <http://maps.google.com> и заставили этот сервис проложить нам маршрут от ЦИФа до зоопарка. И вот что у нас получилось:

![](02.jpg)

И вот так, с хорошим настроением, мы пошли в зоопарк. По дороге мы увидели симпатичный парк с практически круглым озером и зашли его осмотреть. Умилило то, что в парке было две скульптуры: утка

![](03.jpg)

и лось

![](04.jpg)

Утку увековечили в скульптуре не зря, ведь в том озере куча уток, причём их кормят все, кому не лень. Вот так выглядит кусок озера с утками:

![](05.jpg)

После этого мы пошли дальше. И увидели новое удивительное для нас (украинцев) явление:

![](06.jpg)

Причём заведение соответствовало своему названию. Хотя оно было закрыто (в воскресенье в Германии закрыты практически все магазины, кроме некоторых аптек), но сквозь окна было видно великое множество всяких товаров: мониторы, клавиатуры, роутеры... В общем, надо как-нибудь зайти :)

На следующей фотке видно орла. Прикольный орёл, стоит прямо под чьим-то домиком. Обратите внимание также на то, что домик весь увит плющом, но это плющ более "законопослушный", ведь он сбросил листья зимой.

![](07.jpg)

А вот смешная реклама. Правда, я так и не понял, в чём суть телепередачи, которую она рекламирует. Подозреваю, что что-то феминистическое. Но что в таком случае символизирует жирная гусеница - непонятно:

![](08.jpg)

Вот эта реклама была уже очень близко к месту, которое отмечено на карте как "Зоопарк Билефельд". Но оказалось, что "Зоопарк" - это не зоопарк. Точнее, зоопарком непонятно почему назвали магазин. Какой магазин - я не знаю, ведь он был закрыт. В общем, это был облом. Феерический, причём. Поэтому мы, несолоно хлебавши, пошли домой. А я ещё удивлялся, как может зоопарк занимать всего один квартал в центре города. В общем, этот пост - антиреклама карт Гугла. Не верьте им!