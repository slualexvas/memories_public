 Третий раз в Египте -- приезд 
===============================

Я работал-учился и заметил, что упахался. Поэтому решил съездить в Египет, сагитировал с собой маму, чтобы было вдвоём веселее, и выбрали тур.

Мама утверждала, что ей в Шарм-Эль-Шейхе уже надоело, поэтому мы выбрали тур в Марса Алам — ради разнообразия.

Дорога
------

Дорога была ужасная( Вылетали в 2 часа ночи; прилетели в Хургаду в 6 часов утра; в Хургаде нас ждал трансфер до отеля. Всё бы хорошо, если бы не одно «но»: до отеля ехать 4 часа. Отель старался максимально сгладить это обстоятельство и прислал нам комфортную машину с водителем:

![](0.jpg)  

Но всё равно такое большое расстояние — это печально для человека, который почти всю ночь не спал.

Пока нас везли в гостиницу, я смотрел, как живут простые египтяне. В сёлах у них адовый треш: песок, пыль, воды мало, зелени мало...

![Обычный сельский домик](1.jpg "Обычный сельский домик") Обычный сельский домик 

![Очень пафосный сельский магазин (обычный магазин -- это шиферная будка)](2.jpg "Очень пафосный сельский магазин (обычный магазин -- это шиферная будка)") Очень пафосный сельский магазин (обычный магазин -- это шиферная будка) 

![](3.jpg)  

После села мы добрались до города. В городе у египтян вот такие симпатичные многоэтажки:

![](4.jpg)  

![](5.jpg)  

Отельный городок
----------------

Но самая симпатичная местность — она, конечно же, в отельном городке:

![](6.jpg)  

![](7.jpg)  

В этом городке находится сразу несколько отелей, принадлежащих большой корпорации «Marina»:

![](8.jpg)  

Наш отель — один из них; называется он Sunrise Marina Hotel.

Заселение в отель
-----------------

Нам моментально одели браслеты; после этого предложили позавтракать, а они, мол за это время подготовят номер.

Номер подготовили классный:

![](9.jpg)  

Но нам бросилось в глаза отсутствие второй кровати. А мы — мама и сын, мы на одной кровати спать не можем, тк стесняемся. Заселяющий дядька без лишних слов притащил нам вторую кровать.

И только после притаскивания второй кровати нам бросилось в глаза ещё и отсутствие балкона. К сожалению, балкон нам никто не притащил; пришлось договариваться о номере с балконом за дополнительные 30 долларов.

А в номере с балконом была одна двуспальная кровать. Поэтому что? Правильно, поэтому прислуга ещё раз затаскивала вторую кровать к нам в номер))