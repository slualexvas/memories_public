 Трускавец-2022: село "Нагуевичи" 
==================================

Это село является крупным памятником благодаря своему уроженцу — поэту Ивану Франко [https://ru.wikipedia.org/wiki/Франко,\_Иван\_Яковлевич](https://ru.wikipedia.org/wiki/Франко,_Иван_Яковлевич)

Мы посетили двор отца поэта — Якова Франко. Оказалось, что двор весьма неплох, потому что Яков был весьма умелым кузнецом и, как следствие, зажиточным.

На дворе Якова располагались кузня, жилой дом, сарай для животных и сарай для зерна. И ещё сарай для хозяйственных инструментов:

![](0.jpg)  

Первая жена Якова умерла, когда ему было 50 с чем-то лет. Поэтому он женился второй раз на 20-летней девушке — матери поэта. Среди женского пола, присутствующего на экскурсии, этот факт вызвал нездоровое оживление, граничащее с негодованием.

Вот святая святых — кузня:

![](1.jpg)  

Сам поэт вспоминал, что кузня была его любимым местом. В ней всегда было несколько человек, который ждали заказ от кузнеца, и маленький Иван обожал забиться в уголочек и слушать их разговоры. Также он называет кузню «моею главной школой — школой жизни».

Дальше идёт жилой дом. Главное, что поразило меня в доме — это то, что из крыши не торчит дымовая труба:

![](2.jpg)  

Оказалось, причина в том, что Австро-Венгрской империи, которой принадлежали эти земли, существовал налог на дымовые трубы.

Поэтому беднейшие крестьяне выпускали дым прямо в дом, чтобы не платить налог. Они не задыхались, потому что дым быстро выходил сквозь соломенную крышу. Однако всё в доме было грязнющим и провонявшим.

Богатые люди («высший класс») платили налог на дымоход.

Однако средний класс, к которому и принадлежал отец поэта, платить налог не хотели, но и грязнюще-провонявших вещей тоже не желали. Поэтому они шли на такой лайфхак:

![](3.jpg)  

Они строили «дом в доме», и выводили дымоход наружу «внутреннего дома», но внутри крыши. В итоге и вещи не воняют, и налог платить не надо. Лайфхакеры!

Вот фотографии внутреннего убранства хаты:

![](4.jpg)  

![](5.jpg)  

![](6.jpg)  

Однако под крышей жилого дома было две хаты и две печки. То, что сфотографировано выше — это та хата, где жила семья поэта. Однако была и вторая хата — гостевая. В гостевой хате было покрасивше и понаряднее:

![](7.jpg)  

![](8.jpg)  

Ещё там есть собственно музей: с кучей фотографий, с изданными прижизненно книгами поэта, и тд. Фотографии оттуда я выкладывать не буду, потому что людям, не интересующимся плотно жизнью поэта они, на мой взгляд, ничего не дадут.