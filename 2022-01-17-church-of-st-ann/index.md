 Трускавец-2022: поездка в церковь святой Анны в городе Бориславе 
==================================================================

Мы снова вернулись в город нефтяников Борислав и зашли в следующий туристический объект — церковь святой Анны.

Нас встретил тамошний священник — отец Роман. Он рассказал, что церковь была довольно запущенной — ведь в советские времена она не работала как церковь, а работала как склад минеральных удобрений, как похоронное бюро, и тд.

А вот сейчас она очень красочная и богатая. Насколько я понял, это благодаря отцу Роману, который насмотрелся, как люди едут в Рим, Иерусалим и прочие далёкие города, чтобы помолиться над мощами святых.

Чтобы облегчить людям жизнь, он в 2009 году поставил задачу «собирать мощи святых в этой церкви», потому что украинцам до Трускавца ближе, чем до Рима.

Сейчас в церкви находится более тысячи икон и, как я понял, мощей тоже. Чтобы нас поразить, отец Роман у всех спросил, как их зовут, и для каждого нашёл святого с тем же именем. С кем-то это было легко (Леонид, Александр, Наталья, Татьяна). Но ведь с нами была моя жена Карина, а ещё была девочка Даниэлла. И даже для Даниэллы отец Роман сказал, что святая существует, только не выставлена в зале.

Хотя нагуглить такую святую я смог.

Фотографий в церкви я сделал довольно мало, поэтому интересующихся отправляю на сайт: <https://dyvys.info/2017/08/21/do-boryslava-za-dyvom-lyudy-z-yizhdzhayutsya-do-tserkvy-svyatoyi-anny-ztsilytysya-foto-video/>

Но церковь очень красивая, поэтому несколько фотографий я всё-таки выставлю.

Вот две наши фотографии — так выглядит церковь снаружи:

![](0.jpg)  

![](1.jpg)  

А дальше идут уже чужие фотографии:

![](2.jpg)  

![](3.jpg)  

![](4.jpg)  

Верить в Бога или нет? Как относиться к религии?  
Это вопросы, на которые должен ответить каждый.

Но то, что эта церковь очень красивая — это не вопрос. Это непреложный факт.