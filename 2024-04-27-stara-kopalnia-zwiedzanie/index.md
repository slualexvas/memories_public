# Stara Kopalnia - экскурсия с проводником

- Цена экскурсии = 41 злотый с человека
- Время = чуть меньше двух часов

## Комната с марками

Мы начали экскурсию от комнаты с марками. Она по-польски называется "markownia":
- В этой комнате есть трёхгранная колонна с многими тысячами крючков. На каждом крючке написан номер и висит жетончик. 
- Номера на каждой грани дублируются - т.е. номер, скажем, 1072, есть на каждой грани.
- Как только шахтёр поступает на работу, ему присваивается номер, и этот номер не меняется за всё время работы.
- Каждая грань соответствует восьмичасовой смене:
  - На одной грани висят круглые жетончики (это утренняя смена)
  - На второй грани квадратные жетончики (это дневная смена)
  - На третьей грани треугольные жетончики (это ночная смена)
- Если утренняя смена закончилась, а на каком-то крючке нет жетончика - значит, есть подозрение, что человек потерялся в шахте. Следовательно, его будут искать.

![01 - markownia.jpg](01%20-%20markownia.jpg)

Сами марки можно увидеть тут: https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEimDjok0P-g0vJqgEfqvibJDQF39Lt1O2wGagc5i-oSxogagOqcKTOQezGGq1w8tV6NwJqab32YcmUnCWjJ0byjfoawc5zEJimeShY2y4_3iZ97t8HvnnlIvgU_v3SvSTIvlnXXkh8Ow57yhEJkG5xFnc6CL_2lrmZWGMpnlU6B58p2QAqU7YANr-v8-g/s6000/markownia%20stara%20kopalnia%20wa%C5%82brzych.jpg

## Комната с одеждой

По-польски она называется "łaźnia łańcuszkowa". 
- Почему łaźnia (т.е. баня) - потому что люди здесь переодевались и мылись
- Почему łańcuszkowa - потому что с потолка свисали цепи, и люди свою одежду вешали на специальный крюк, и потом поднимали на цепи вверх. Благодаря этому можно было в ограниченном пространстве хранить одежду нескольких тысяч людей.
- Возле каждой цепи был выбит номер. Это тот же номер, который был на марке.
- В этой бане стоял жуткий запах:
  - Угольная пыль
  - Метан (Валбжихские шахты генерировали очень много газов)
  - Пот, грязь
  - Курево (шахтёрам не разрешали курить в шахтах, поэтому они выкуривали полпачки в раздевалке перед сменой, а докуривали пачку после смены)

Фотка не валбжихской, а другой комнаты. Валбжихскую я толком не нашёл:

![02-łaźnia-łańcuszkowa.jpg](02-%C5%82a%C5%BAnia-%C5%82a%C5%84cuszkowa.jpg)

## Экспонат "технология шахты как водяного канала"

Экскурсовод рассказал, что в 1700-х годах какой-то немецкий граф придумал такую технологию:
- Если шахта находится в горе, вровень с поверхностью
- То подводим к горе канал
- Проводим канал под горой
- Пускаем по каналу лодки
- Лодки загружаются углём и выплывают наружу

Так делали и в Валбжихе.

На следующей фотке видно схему, как выглядит озеро и канал возле горы. Прошу обратить внимание на причал и кучу угля:

![03-1-technologia-woda-w-kopalnie.jpg](03-1-technologia-woda-w-kopalnie.jpg)

А вот как выглядит канал внутри горы (на экспонате изображена гора в разрезе):

![03-2-mountain-inner.jpg](03-2-mountain-inner.jpg)

## Экспонат "пульт дежурного"

Следующий экспонат - не из 1700-х, а из 1990-х годов: пульт дежурного по шахте:
- По всей шахте были датчики газов (метана, CO, CO_2).
- Датчики были связаны проводами с этим пультом. 
- Дежурный сидел и смотрел на пульт круглосуточно. Как только он замечал, что загорелась красная лампочка, так сразу он по телефону звонил и объявлял: "штольня номер N, у вас превышение метана!"

![04-pult.jpg](04-pult.jpg)

## Стойка с наборами для дыхания

После раздевалки шахтёр проходил к нескольким стойкам, где были нужные для него вещи. Первая нужная вещь - это устройство для дыхания в случае выброса газов. Оно состояло из:
- Дыхательной маски
- Трубки
- Баллона с кислородом

Такого баллона хватало на час дыхания.

Это - экстренное устройство; рабочий должен его использовать только в случае, когда ему нечем дышать.

![05-balloons-with-oxygen.jpg](05-balloons-with-oxygen.jpg)

## Стойка с лампами

Следующая стойка - с шахтёрскими лампами:
- Лампа питалась от аккумулятора
- Прямо в стойке были провода от зарядного устройства. Следовательно, когда шахтёр отработал, то он лампу ставил на зарядку
- В каждой стойке (и лампы, и наборы для дыхания) были номерки, соответствующие номерам с марок

![06-lamps.jpg](06-lamps.jpg)

А вот так выглядели лампы, когда ещё не перешли на электрическое освещение:

![06-lamps-old.jpg](06-lamps-old.jpg)

## Экипированный шахтёр

Вот так выглядел шахтёр, посетивший стойки:
- В красной сумке - набор для дыхания
- В чёрной сумке - аккумулятор для лампы
- На каске - лампа

![07-equipped-miner.jpg](07-equipped-miner.jpg)

## Ответвление: праздник 04 декабря

04 декабря - это день святой Варвары, покровительницы шахтёров. 
- В этот день никто в шахту не спускался, а шли на праздник.
- Праздники были гендерно разделены: мужской праздник назывался "Korczma piwna", а женский - не помню.
- На мужском празднике использовалась не рабочая форма шахтёров, а специальные парадные мундиры.
- Шапки украшались перьями. Цвет перьев указывал на должность:
  - Зелёные перья = директор
  - Красные перья = заместители
  - Белые перья = не помню
  - Чёрные перья = не помню

![08-miner-festiwal-clothes.jpg](08-miner-festiwal-clothes.jpg)

На следующей фотке видим невольничьи колодки (справа) и коллекцию кружек (слева):

![08-miner-festiwal-kettles.jpg](08-miner-festiwal-kettles.jpg)

Невольничьи колодки - это шуточная штука. Если шахтёр вёл себя "невежливо" (чаще всего это означает "выпил пиво, не дослушав тост"), то его заковывали в колодки и прописывали десять ударов по ягодицам. Удары раздавали чёрной лопаткой, которую видно на фото. На лопатке написано "dupochlast".

Насчёт коллекции кружек - тут история поинтереснее:
- Каждый год шахта выбирала какое-то знаковое событие.
- В честь этого события шахта производила много одинаковых кружек. Например:
  - В честь 1989 года, когда в Польше ввели военное положение, произведены кружки в форме танка
  - В честь последнего года работы шахты - 1996 - произведены кружки в форме гроба

## Картография

На шахтах была очень развита картография. Делали всё очень старательно, чтобы чётко понимать - куда и как двигаться. Использовались инструменты:
- Компас шахтёрский
- Теодолит (инструмент для измерения углов на местности и прокладывания прямых линий)
- Нивелир (инструмент для измерения разницы уровня высот)
- Лата измерительная (полосатая палка по типу жезла гаишника, но длиной в метр. Каждое деление означало какую-то часть метра)

## Взрыватель

Иногда в шахтах использовали взрывы. На фото ниже представлен электрический взрыватель для них. На польском языке он называется "zapalarka typu ZK-300":

![09-zapalarka-typ-zk-300.jpg](09-zapalarka-typ-zk-300.jpg)

## Верх шахтного подъёмника

После этого мы пришли на верх шахтного подъёмника. Это - что-то вроде современного лифта, которое поднимало-опускало шахтёров и уголь (идею с лодками давно уже забросили; ещё с тех пор, как начали добывать уголь ниже уровня земли). Вот модель подъёмника:

![10-lift-model.jpg](10-lift-model.jpg)

А вот наш экскурсовод показывает, как модель работает: https://youtube.com/shorts/UPbHCj5dCKk

А вот список сигналов:

![10-lift-signals.jpg](10-lift-signals.jpg)

Сигналы нужно было подавать, стукая молотком по круглой штуке на фото. Эта штука металлическая, и очень громко звенит.

А на следующем фото видим рельсы:

![10-lift-rails.jpg](10-lift-rails.jpg)

По этим рельсам можно было выкатить вагонетку угля прямо из лифта и покатить её дальше для разгрузки.

Чуть позже мы вышли под открытое небо, и там была кабина лифта:

![10-lift-cabine.jpg](10-lift-cabine.jpg)

## Сортировка угля

На следующем фото изображён конвейер, который вёз вагонетки и переворачивал их:

![11-carbon-sorting.jpg](11-carbon-sorting.jpg)

Сортировка угля состояла из многих этапов, я запомнил два из них:
- просеивание (т.е. разделить уголь по размерам)
- флотация:
  - Используется для угольной пыли
  - Пыль смешивают с водой, получая жижу
  - В жижу добавляют специальный реагент, который смачивает уголь, но отталкивает ненужные породы (песок, глину, и тд)
  - Через жижу пропускают воздух, чтобы получались пузырьки
  - Уголь проникает внутрь пузырьков, и поэтому поднимается в верхнюю часть жижи, а ненужные породы оседают на дно

## Машинный зал

Потом нас привели в машинный зал. Раньше были паровые машины, но, по-моему, в 1914 году установили электрический двигатель. Эти машины приводили в движение подъёмник (их две, но я сфоткал одну):

![12-machine.jpg](12-machine.jpg)

Управлял машиной человек, он назывался "оператор". Сидел оператор в специальной будочке рядом с машиной:

![12-machine-opetator-house.jpg](12-machine-opetator-house.jpg)

Перед оператором была куча стрелок, а также вертикальная чёрная рейка, по которой передвигались два красных указателя. Эти указатели находились в том же месте, что и кабины лифта на подъёмнике:

![12-machine-operator-pointers.jpg](12-machine-operator-pointers.jpg)

В отдельной комнате был преобразователь - он превращал переменный ток в постоянный, который и питал указанные машины:

![12-transformator.jpg](12-transformator.jpg)

Постоянный ток был нужен потому, что в то время ещё не умели уменьшать скорость машины, используя переменный ток. А вот используя постоянный - умели.

Также я сфоткал табличку, которая была на трансформаторе. В частности, из таблички видно, что мощность этой бандуры превышает один мегаватт:

![12-transformator-table.jpg](12-transformator-table.jpg)

## Шахта

Потом мы спустились в шахту (очень неглубоко: метров, наверное, 10 под землю). 

Первое, что нам показали - это инструменты, используемые шахтёрами:

![13-mine-instruments.jpg](13-mine-instruments.jpg)

Слева на фотке видно лопату в форме сердца. Её так и называют "лопата-сердцувка". 110 таких лопат угля = тонна угля.

А вот так выглядела сама шахта:

![13-mine-main.jpg](13-mine-main.jpg)

Потом мы зашли в более дальнюю часть шахты, где свод уже не был залит бетоном. Там было видно, что свод состоит из железных полукругов, а каждый полукруг - из трёх одинаковых железных дуг, скреплённых болтами:

![13-mine-iron-arcs.jpg](13-mine-iron-arcs.jpg)

На эти дуги клались деревянные брусья.

А вот информация, посвящённая шахтному транспорту:

![13-mine-transport.jpg](13-mine-transport.jpg)

А вот элемент местного фольклора - подземный дух:

![13-cave-soul.jpg](13-cave-soul.jpg)

Если в пустой шахте слышишь какие-то звуки - беги, это идёт подземный дух. Если не убежал - жди обвала или других неприятностей.

## Более отдалённая часть шахты

В следующей части шахты первое, что мы увидели - это рабочего с лютней. Лютня - это здоровенная труба, служащая для вентиляции, когда ещё шахта свежепродолбанная, и в неё не провели ещё стационарную вентиляцию. Труба состоит из частей, и такую часть держит рабочий на фото:

![14-deeper-mine-lutnia.jpg](14-deeper-mine-lutnia.jpg)

Раньше, когда ещё не было железных труб, использовали железно-матерчатые трубы:

![14-deeper-mine-lutnia-old.jpg](14-deeper-mine-lutnia-old.jpg)

Таблица, посвящённая борьбе с загазованностью шахт:

![14-deeper-mine-table-gases.jpg](14-deeper-mine-table-gases.jpg)

Маленький выброс газа в шахту из стены. Когда он маленький, то называется "Фукач" (ударение на "у"):
- Фото не удалось, поэтому опишу словами:
  - Это дыра в стене, диаметром приблизительно полметра
  - за дырой видна узенькая пещера, ведущая где-то далеко
  - В этой пещере, как я понимаю, был газ
  - Этот газ где-то давило-давило, и в итоге сдавило так, что он пробил дырочку в стене шахты (мало ли, откатил какой-то камешек) и "фукнул" в шахту.

А вот таблица со схемой вентиляции шахты:

![14-deeper-mine-ventilation.jpg](14-deeper-mine-ventilation.jpg)

Лодочка - как я подозреваю, ещё из 1700-х годов:

![14-deeper-mine-boat.jpg](14-deeper-mine-boat.jpg)

## Забой

И вот мы подошли собственно к забою. Эта часть шахты ниже и Уже, чем предыдущие:

![15-coalface.jpg](15-coalface.jpg)

Машина, поставляющая уголь от шахтёров к вагонеткам:

![15-coalface-machine.jpg](15-coalface-machine.jpg)

И вот собственно шахтёры в забое:

![15-coalface-miners.jpg](15-coalface-miners.jpg)

Шахтёры работали парами:
- Один рубильщик (т.е. рубит уголь)
- Второй отбрасывальщик (т.е. отбрасывает нарубленный уголь на транспортировочную машину)

## Измерители

Измерители - это особый род профессий, которые предоставляют информацию для картографов. 

На фото ниже изображён мерщик с теодолитом:

![16-measurer-with-teodolit.jpg](16-measurer-with-teodolit.jpg)

А на следующем фото виден помощник мерщика. Он держит лату - палку с делениями:

![16-measurer-with-lata.jpg](16-measurer-with-lata.jpg)

## Поверхность

После этого мы вышли из шахты на поверхность и пошли к последнему пункту нашей экскурсии - высокой башне для обозрения окрестностей.

С той башни я записал видео: https://youtu.be/cNX9u6-ZJJ8

Приглашаю на этом видео найти терриконы и коксовню (коксовня выглядит как навес, под которым куча угля). Кстати, что интересно:
- шахта закрыта с 1996 года
- а вот коксовня всё ещё действует; на неё возят уголь с какой-то другой шахты из другого города.

## Кофейня и кулон

Потом мы с Кариночкой сходили в местную кофейню под названием "Sztygarówka" (штыгарувка). Штыгар - это работник шахты, который занимается надзором техническим и частично администрационным. В кофейне ничего особенного, поэтому фотки не выкладываю.

Но зато на ресепшне, где можно было купить билеты, продавались также сувениры - украшения с углём. Я купил для Кариночки кулон за 20 злотых:

![17-carbon-culon.jpg](17-carbon-culon.jpg)

## P.S.

https://www.youtube.com/watch?v=lPLTBLcvmQw - здесь видно, как идёт работа в шахте

## P.P.S. 

За 50 лет (с 1945 по 1995) в этой шахте погибло 1100 шахтёров.

