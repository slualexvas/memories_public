<?php
require_once __DIR__ . '/recursiveProcessDirectory.function.php';

if (!isset($argv[1])) {
    die("usage: {$argv[0]} path/to/dir");
}

recursiveProcessDirectory(__DIR__ . '/' . $argv[1], function ($filePath) {
    $newPath = str_replace('_original.jpg', '.jpg', $filePath);
    rename($filePath, $newPath);
});