<?php
require_once __DIR__ . '/recursiveProcessDirectory.function.php';

recursiveProcessDirectory(
    $argv[1],
    function (string $filePath) {
        if (str_ends_with($filePath, '.png')) {
            system("mogrify -format jpg {$filePath}");
            unlink($filePath);
        }
    }
);