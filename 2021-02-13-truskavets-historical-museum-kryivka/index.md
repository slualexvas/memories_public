 Трускавецкий исторический музей: экспозиция "Криївка" 
=======================================================

Криївка (по-русски «Крыивка») — это разновидность землянки. Подобные землянки устраивали в Карпатах украинские националисты (бандеровцы), когда боролись против советской власти в 1940-1960 годах.

Главное в крыивке — это сделать её незаметной. Например, на люке от крыивки насыпали 20-30 см земли, сверху клали дёрн и тщательно поливали, чтобы, не дай Бог, дёрн не выделялся. Особо упоротые ещё и высаживали куст. Но я лично в это не верю, тк думаю, что куст засохнет.

Также дым от печи выводили через разветвлённую трубу в несколько мест, чтобы его было потруднее увидеть и унюхать.

Музей заморочился и воспроизвёл подобную землянку:

![](0.jpg)  

![](1.jpg)  

![](2.jpg)  

![](3.jpg)  

![](4.jpg)  

![](5.jpg)  

![](6.jpg)  

![](7.jpg)  

![](8.jpg)  

![](9.jpg)  

![](10.jpg)  

![](11.jpg)  

![](12.jpg)  

Кроме интерьера, вещей и оружия, интересны также листовки украинских националистов:

![](13.jpg)  

![](14.jpg)  

![](15.jpg)  

![](16.jpg)  

![](17.jpg)  

![](18.jpg)