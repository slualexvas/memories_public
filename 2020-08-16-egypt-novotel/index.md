 Египет: отель "NOVOTEL MARSA ALAM" 
====================================

### Общие данные

Отель, в который мы поехали, называется "Novotel". Подробнее можно почитать здесь: <https://www.booking.com/hotel/eg/novotel-marsa-alam.uk.html>.

### Номер

Номер очень красив и выполнен в морском стиле:

![](0.jpg)  

![](1.jpg)  

![](2.jpg)  

![](3.jpg)  

![](4.jpg)  

![](5.jpg)  

![](6.jpg)  

На последней фотографии слева от двери есть маленький кармашек. В этот кармашек нужно совать карточку, которая служит ключём (электронным) от двери. Если карточку в кармашек не сунуть, то в номере отключается электричество. Т.е. идея в том, чтобы туристы добровольно-принудительно выключали свет, когда уходят из номера. А когда турист в номере, то он всунет карточку в кармашек, и она не потеряется. Единственный шанс всё ж таки потерять карточку - это поселиться вдвоём.

Кариночка, правда, не преминула потестить, и всунула карточку от АромаКава (это наша киевская кофейня). Оказывается, работает любая карточка.

### Питание

По питанию было большое огорчение, связанное с тем, что шведский стол отменили из-за коронавируса. Поэтому, когда мы только-только приехали, то питание было таким:

1. На столе лежит меню: два супа, три гарнира, три мяса/рыбы, четыре салата, три десерта.
2. К тебе подходит официант и спрашивает, что ты пожелаешь. Ты имеешь право выбрать что-то одно из каждого вида еды. Например, суп такой-то, гарнир такой-то, к гарниру рыбу. Или говядину. Или курицу. Салат такой-то, десерт такой-то.
3. Взять, например, курицу и рыбу одновременно ты не имеешь права. Отказаться от чего-то тоже не имеешь права; его всё равно принесут. Хотя можно не есть.

Ели мы в итальянском ресторане Al Dente:

![](7.jpg)  

Вот так выглядел приём пищи (десерт ещё не принесли):

![](8.jpg)  

Но потом приехало больше туристов, официанты упахались, и еду перевели ближе к классическому шведскому столу. Конкретнее: блюда раскладывались, как для обычного шведского стола, но все они накрывались плёнкой. Ты ходил и тыкал пальцем, а обслуживающий персонал накидывал еду тебе на тарелку.

Вот так выглядел завтрак после такого "почти шведского стола":

![](9.jpg)  

### Территория

Моя невеста Кариночка сняла хороший таймлапс про территорию отеля. Вот он:

https://www.youtube.com/watch?v=3dWBwo9gbaU Если же вам комфортнее воспринимать информацию по фотографиям, то вот она:

### Отель днём:

![](10.jpg)  

![](11.jpg)  

![](12.jpg)  

![](13.jpg)  

![](14.jpg)  

А вот бар в отеле:

![](15.jpg)  

### Отель ночью

![](16.jpg)  

![](17.jpg)  

![](18.jpg)  

![](19.jpg)  

![](20.jpg)  

Флора отеля
-----------

![](21.jpg)  

![](22.jpg)  

![](23.jpg)  

![](24.jpg)  

![](25.jpg)  

![](26.jpg)  

![](27.jpg)  

![](28.jpg)  

Отдельно хочу обратить внимание на финики на пальмах.

### Фауна отеля

Гуляя вечером, встретили вот такого кузнечика длиной сантиметров в десять:

![](29.jpg)  

![](30.jpg)  

В один из предыдущих дней встретили и сумели заснять геккона, тоже длиной сантиметров в десять:

![](31.jpg)  

![](32.jpg)  

### Сервис

Меня умилил сервис в отеле, и несколькими эпизодами хочу поделиться.

**Эпизод 1:** На пляже был красный флаг (т.е. купаться нельзя). Мы шли мимо ресепшна, и у нас спросили "всё ли хорошо". Я честно сказал, что "всё хорошо, вот бы ещё флаг был жёлтый". И тут, абсолютно неожиданно для меня, сотрудник отеля заявил "я поговорю с директором, и если ветер хоть чуть-чуть ослабнет, заменим флаг на жёлтый сегодня". И действительно, они где-то через полтора часа заменили флаг на жёлтый.

**Эпизод 2:** Когда ужин перенесли из одного ресторана в другой, то сотрудники не поленились, прошлись по всем номерам и предупредили. А кого не предупредили, тому воткнули в дверь записку.

**Эпизод 3 (шуточный):** Рядом с нами поселился араб, который курил что-то вонючее (непохожее на табак). Когда на следующее утро мы пошли на ресепшн (мы хотели переселиться в другой номер), то на ресепшне нам заявили "этот сосед сегодня же съедет". Я думаю, что он съезжал сам по себе, но в глубине души приятнее думать, что его выперли из-за наших жалоб :)

