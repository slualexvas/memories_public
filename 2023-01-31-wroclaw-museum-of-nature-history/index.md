# Вроцлавский музей естественной истории

Даже лестница перед входом в музей очень красивая:

![stairs-to-museum.jpg](stairs-to-museum.jpg)

Мы купили билеты (по 14 злотых с человека, итого 28) и пошли обозревать экспозицию.

## Часть 1: отдельная экспозиция "насекомые и человек"

Начинается экспозиция с шелкопряда:

![silk-worm.jpg](part-1-insects/silk-worm.jpg)

После этого есть две пластиковые фигурки, на которых написано, что "меня можно погладить!". Одна фигурка изображает гаммаруса, а вторая - водяного скорпиона:

![gammarus.jpg](part-1-insects/gammarus.jpg)

![water-skorpion.jpg](part-1-insects/water-skorpion.jpg)

Следующий стенд посвящен преображениям насекомых (имеются в виду преображения "яйцо - гусеница - куколка - взрослая особь"):

![metamorfoza-1.jpg](part-1-insects/metamorfoza-1.jpg)

![metamorfoza-2.jpg](part-1-insects/metamorfoza-2.jpg)

Дальше идёт стенд про блестящих насекомых. Странные рисунки в центре - это чешуя насекомых под микроскопом:

![shine.jpg](part-1-insects/shine.jpg)

Следующий стенд посвящён разнообразию окраски насекомых:

![colours.jpg](part-1-insects/colours.jpg)

А ещё на одном стенде рисунок крыльев бабочки стараются рассмотреть и классифицировать:

![butterfly-colors.jpg](part-1-insects/butterfly-colors.jpg)

На следующем стенде представлены воробей и синица. Под каждой из птиц на булавках наколот ейный рацион, причём наколот в форме треугольника:

![birds-eat.jpg](part-1-insects/birds-eat.jpg)

И проценты посчитаны:

![bird-eat-percents.jpg](part-1-insects/bird-eat-percents.jpg)

После птичьего рациона мы переходим к пчеле медоносной:

![bee.jpg](part-1-insects/bee.jpg)

А от пчелы переходим к малюсеньким родственницам ос. Эти самые родственницы откладывают яйца на черешки листьев дуба, и от этого на дубе появляются наросты, похожие на ягоды. Из этих ягод добывают особый сок, который можно использовать для производства чернил и для дубления кожи:

![galasowki.jpg](part-1-insects/galasowki.jpg)

![galasowki-2.jpg](part-1-insects/galasowki-2.jpg)

На следующем стенде рассказывается про "кошениль". Кошениль - это натуральный краситель красного цвета, который добывается из двух видов червяков - "червец кактусовый" и "червец польский":

![czerwec-kaktusowy.jpg](part-1-insects/czerwec-kaktusowy.jpg)

![czerwec-polski.jpg](part-1-insects/czerwec-polski.jpg)

А есть ещё "червец шеллаковый". Колонии этого червеца покрывают ветку; с ветки люди его как-то соскребают, что-то делают, и получается лак под названием "шеллак". Потом этот самый "шеллак" растворяют в спирте и получается жидкость под названием "политура". А если этой самой политурой помазать дерево, то получится лакированное дерево:

![shellak.jpg](part-1-insects/shellak.jpg)

## Часть 2 "Животные"

Основная выставка начиная с плаката "история музея". Текст плаката продублирован здесь: http://www.muzeum-przyrodnicze.uni.wroc.pl/en/index.php?go=history

Вкратце:
- Музей естественной истории возник в результате слияния прежних зоологического и ботанического музеев (а также гербария).
- Зоологический музей возник в 1814 году вскоре после учреждения светского Вроцлавского университета, бывшего тогда немецким (1814).
- Наиболее заслуженные учёные, чьи коллекции дожили до наших дней: Адольф Эдуард Грубе, Карл Чун, Вилли Кекенталь, Фердинанд Пакс-младший.
- Во время Второй мировой войны, когда Вроцлав был осажден (Festung Breslau), здание подверглось бомбардировке. Все его крыло обрушилось, а экспозиции, кроме зала скелетов, полностью разорены, как и помещения, предназначенные для дидактических целей. Около 50% научных коллекций пережили войну, после чего музей перешел к властям польского университета.

Также посреди зала есть огромный скелет. Это скелет голубого кита, причём не взрослого кита, а всего лишь детёныша.

"Голубой" - это видовое название, а не сексуальная ориентация.

![whale-skeleton.jpg](part-2-animals/whale-skeleton.jpg)

Также есть вот такая интересная таблица:

![count_taxonomy.jpg](part-2-animals/count_taxonomy.jpg)

Согласно этой таблице, на Земле больше всего видов моллюсков (Mięczaki). На втором месте хордовые (strunowce), а на третьем месте нематоды, т.е. черви (по-польски nicienie).

### Кораллы

![1.jpg](part-2-animals/corals/1.jpg)

![2.jpg](part-2-animals/corals/2.jpg)

![3.jpg](part-2-animals/corals/3.jpg)

![4.jpg](part-2-animals/corals/4.jpg)

### Губки

![gubki.jpg](part-2-animals/corals/gubki.jpg)

### Отдельный плакат "Животные в балтийском янтаре"

![animals-in-baltic-amber.jpg](part-2-animals/animals-in-baltic-amber.jpg)

### Кишечнополостные

Кишечнополостные можно разделить на две разновидности.

Первая разновидность - это те, которые прилепились концом "стебля" к твёрдой поверхности и машут щупальцами в воде:

![1.jpg](part-2-animals/coelenterata/1.jpg)

![2.jpg](part-2-animals/coelenterata/2.jpg)

![3.jpg](part-2-animals/coelenterata/3.jpg)

Вторая разновидность - это те, кто свободно плавают в воде. Это медузы:

![4.jpg](part-2-animals/coelenterata/4.jpg)

Многие медузы белые; но есть и вот такая розовая красавица из Карибского моря:

![5.jpg](part-2-animals/coelenterata/5.jpg)

### Моллюски

Кальмар:

![calmar.jpg](part-2-animals/clams/calmar.jpg)

Каракатица:

![karakatitsa.jpg](part-2-animals/clams/karakatitsa.jpg)

Осьминог:

![octopus.jpg](part-2-animals/clams/octopus.jpg)

Непонятно кто:

![unknown.jpg](part-2-animals/clams/unknown.jpg)

Кроме того, в музее есть экспозиция раковин моллюсков:

![1.jpg](part-2-animals/clams/shells/1.jpg)

![2.jpg](part-2-animals/clams/shells/2.jpg)

![3.jpg](part-2-animals/clams/shells/3.jpg)

![4.jpg](part-2-animals/clams/shells/4.jpg)

Обратите внимание на узор из треугольников на следующей раковине:

![5.jpg](part-2-animals/clams/shells/5.jpg)

А вот эти раковины очень красивы. Я когда их увидел, то понял, почему туземные женщины использовали их как украшения:

![6.jpg](part-2-animals/clams/shells/6.jpg)

![7.jpg](part-2-animals/clams/shells/7.jpg)

Также изображена внутренняя структура раковины:

![8.jpg](part-2-animals/clams/shells/8.jpg)

### Членистоногие

#### Ракообразные

Раки:

![crayfish.jpg](part-2-animals/arthropoda/Crustacea/crayfishs.jpg)

Крабы:

![crabs.jpg](part-2-animals/arthropoda/Crustacea/crabs.jpg)

Крабы в раковинах:

![crabs-in-shells.jpg](part-2-animals/arthropoda/Crustacea/crabs-in-shells.jpg)

Не пойми что: (или это креветки?)

![other.jpg](part-2-animals/arthropoda/Crustacea/other.jpg)

На этой фотке есть ракообразные ("старораки"), но есть и паукообразные, которых мы рассмотрим следующими:

![staroraki.jpg](part-2-animals/arthropoda/Crustacea/staroraki.jpg)

#### Паукообразные

![1.jpg](part-2-animals/arthropoda/arahnids/1.jpg)

#### Насекомые

![1.jpg](part-2-animals/arthropoda/insects/1.jpg)

![2.jpg](part-2-animals/arthropoda/insects/2.jpg)

Есть такая разновидность насекомых, которая маскируется под стебли и палки. Они так и называются "палочники":

![3.jpg](part-2-animals/arthropoda/insects/3.jpg)

Кузнечики, саранча и их родственники:

![4.jpg](part-2-animals/arthropoda/insects/4.jpg)

##### Жуки

![1.jpg](part-2-animals/arthropoda/insects/bugs/1.jpg)

![2.jpg](part-2-animals/arthropoda/insects/bugs/2.jpg)

![3.jpg](part-2-animals/arthropoda/insects/bugs/3.jpg)

![4.jpg](part-2-animals/arthropoda/insects/bugs/4.jpg)

![5.jpg](part-2-animals/arthropoda/insects/bugs/5.jpg)

##### Бабочки

![1.jpg](part-2-animals/arthropoda/insects/butterflys/1.jpg)

![2.jpg](part-2-animals/arthropoda/insects/butterflys/2.jpg)

![3.jpg](part-2-animals/arthropoda/insects/butterflys/3.jpg)

![4.jpg](part-2-animals/arthropoda/insects/butterflys/4.jpg)

![5.jpg](part-2-animals/arthropoda/insects/butterflys/5.jpg)

![6.jpg](part-2-animals/arthropoda/insects/butterflys/6.jpg)

### Хордовые

#### Рыбы

![1.jpg](part-2-animals/vertebrates/fishes/1.jpg)

![2.jpg](part-2-animals/vertebrates/fishes/2.jpg)

![3.jpg](part-2-animals/vertebrates/fishes/3.jpg)

![4.jpg](part-2-animals/vertebrates/fishes/4.jpg)

#### Рептилии

![crocodile.jpg](part-2-animals/vertebrates/reptiles/crocodile.jpg)

![gecko.jpg](part-2-animals/vertebrates/reptiles/gecko.jpg)

Игуана:

![iguana.jpg](part-2-animals/vertebrates/reptiles/iguana.jpg)

Хамелеон:

![cameleo.jpg](part-2-animals/vertebrates/reptiles/cameleo.jpg)

Варан нильский:

![waran-nilowy.jpg](part-2-animals/vertebrates/reptiles/waran-nilowy.jpg)

Питон королевский:

![python-royal.jpg](part-2-animals/vertebrates/reptiles/python-royal.jpg)

Разнообразные черепахи:

![1.jpg](part-2-animals/vertebrates/reptiles/turtles/1.jpg)

![2.jpg](part-2-animals/vertebrates/reptiles/turtles/2.jpg)

![3-aligatorowa.jpg](part-2-animals/vertebrates/reptiles/turtles/3-aligatorowa.jpg)

![4.jpg](part-2-animals/vertebrates/reptiles/turtles/4.jpg)

![5.jpg](part-2-animals/vertebrates/reptiles/turtles/5.jpg)

![6.jpg](part-2-animals/vertebrates/reptiles/turtles/6.jpg)

#### Земноводные

Жаба рогатая:

![horned-frog.jpg](part-2-animals/vertebrates/amphibia/horned-frog.jpg)

#### Птицы

##### Страусы

Киви:

![kiwi.jpg](part-2-animals/vertebrates/birds/strauses/kiwi.jpg)

Эму:

![emu.jpg](part-2-animals/vertebrates/birds/strauses/emu.jpg)

Нанду:

![nandu.jpg](part-2-animals/vertebrates/birds/strauses/nandu.jpg)

Казуар:

![kazuar.jpg](part-2-animals/vertebrates/birds/strauses/kazuar.jpg)

Обычный страус:

![straus.jpg](part-2-animals/vertebrates/birds/strauses/straus.jpg)

##### Куроподобные

Чубач:

![czubacz.jpg](part-2-animals/vertebrates/birds/chicken-like/czubacz.jpg)

Ольшняк гималайский:

![olsniak-gimalaiskyi.jpg](part-2-animals/vertebrates/birds/chicken-like/olsniak-gimalaiskyi.jpg)

Базант диаментовий:

![bazant-diamentowyi.jpg](part-2-animals/vertebrates/birds/chicken-like/bazant-diamentowyi.jpg)

Перлица:

![perlice.jpg](part-2-animals/vertebrates/birds/chicken-like/perlice.jpg)

Дрофа:

![drofa.jpg](part-2-animals/vertebrates/birds/chicken-like/drofa.jpg)

##### Водные птицы

Нури:

![nury.jpg](part-2-animals/vertebrates/birds/water/nury.jpg)

Пингвины:

![penguins.jpg](part-2-animals/vertebrates/birds/water/penguins.jpg)

Альбатрос:

![albatros.jpg](part-2-animals/vertebrates/birds/water/albatros.jpg)

По-польски эта птица называется "Глуптак", по-украински "сула", по-русски "Олуша" (https://ru.wikipedia.org/wiki/Северная_олуша):

![gluptak.jpg](part-2-animals/vertebrates/birds/water/gluptak.jpg)

Аистовые:

![stork.jpg](part-2-animals/vertebrates/birds/water/stork.jpg)

Фламинго:

![flamingo.jpg](part-2-animals/vertebrates/birds/water/flamingo.jpg)

Бекас:

![bekas.jpg](part-2-animals/vertebrates/birds/water/bekas.jpg)

![bekas-2.jpg](part-2-animals/vertebrates/birds/water/bekas-2.jpg)

Журавль ([Википедия](https://ru.wikipedia.org/wiki/Журавли) говорит, что журавль выглядит совсем не так):

![zurawie.jpg](part-2-animals/vertebrates/birds/water/zurawie.jpg)

Лыска:

![lyska.jpg](part-2-animals/vertebrates/birds/water/lyska.jpg)

По-польски эта разновидность называется "Długoszpony (длиннокогтевые)", а по-русски "Яканы":

![dlugoszpony.jpg](part-2-animals/vertebrates/birds/water/dlugoszpony.jpg)

А вот следующая птица имеет весьма говорящее название "Крабожер":

![crab-eater.jpg](part-2-animals/vertebrates/birds/water/crab-eater.jpg)

Эта симпатяга называется "Алки":

![alki.jpg](part-2-animals/vertebrates/birds/water/alki.jpg)

##### Хищные птицы

Кондор:

![condor.jpg](part-2-animals/vertebrates/birds/predators/condor.jpg)

Кобчик:

![kobchik.jpg](part-2-animals/vertebrates/birds/predators/kobchik.jpg)

##### Семейство "Кукушковые":

Эта птица по-польски называется "owocozer". Учитывая, что "owoce" переводится с польского как "фрукты", то я бы перевёл название птицы как "фруктоед":

![owocozer.jpg](part-2-animals/vertebrates/birds/kukulidae/owocozer.jpg)

А вот эту птицу назвали ласково: "Кукушечка золотистая":

![kukuleczka-zlocista.jpg](part-2-animals/vertebrates/birds/kukulidae/kukuleczka-zlocista.jpg)

Koroniec siodłaty:

![Koroniec-siodlaty.jpg](part-2-animals/vertebrates/birds/kukulidae/Koroniec-siodlaty.jpg)

Вот ещё красавчики:

![krasavchiki.jpg](part-2-animals/vertebrates/birds/kukulidae/krasavchiki.jpg)

Вот эта красавица водится в Индонезии:

![kukula.jpg](part-2-animals/vertebrates/birds/kukulidae/kukula.jpg)

##### Попугаи:

Какаду:

![kakadu.jpg](part-2-animals/vertebrates/birds/parrots/kakadu.jpg)

Лора:

![lora.jpg](part-2-animals/vertebrates/birds/parrots/lora.jpg)

Попугай "арарауна" гордо демонстрирует цвета украинского флага. Может, он выведен в биолабораториях?

![ararauna.jpg](part-2-animals/vertebrates/birds/parrots/ararauna.jpg)

Ара жёлтокрылая:

![ara-yellow-wing.jpg](part-2-animals/vertebrates/birds/parrots/ara-yellow-wing.jpg)

Ара голубая:

![ara-blue.jpg](part-2-animals/vertebrates/birds/parrots/ara-blue.jpg)

Завершу раздел с попугаями вот этим маленьким попугайчиком по имени "конура солнечная":

![konura-sloneczna.jpg](part-2-animals/vertebrates/birds/parrots/konura-sloneczna.jpg)

##### Остальные птицы:

![blawatnik.jpg](part-2-animals/vertebrates/birds/other/blawatnik.jpg)

![lirogon.jpg](part-2-animals/vertebrates/birds/other/lirogon.jpg)

![dziwogon.jpg](part-2-animals/vertebrates/birds/other/dziwogon.jpg)

![remez.jpg](part-2-animals/vertebrates/birds/other/remez.jpg)

##### Отдельная небольшая комнатка "яйца и гнёзда":

![1.jpg](part-2-animals/vertebrates/birds/eggs/1.jpg)

Чайка:

![czaika.jpg](part-2-animals/vertebrates/birds/eggs/czaika.jpg)

Какая-то "cieweczka-rzeczna":

![cieweczka-rzeczna.jpg](part-2-animals/vertebrates/birds/eggs/cieweczka-rzeczna.jpg)

Вальдшнеп:

![scolopax.jpg](part-2-animals/vertebrates/birds/eggs/scolopax.jpg)

#### Млекопитающие

Начинается список млекопитающих с утконоса и ехидны. По-польски они называются "дзьобак" и "кольчатка":

![dziobak.jpg](part-2-animals/vertebrates/mammalia/dziobak.jpg)

![kolczatka.jpg](part-2-animals/vertebrates/mammalia/kolczatka.jpg)

Крыса-кенгуру:

![kenguru-czur.jpg](part-2-animals/vertebrates/mammalia/kenguru-czur.jpg)

Вомбат:

![wombat.jpg](part-2-animals/vertebrates/mammalia/wombat.jpg)

Этот товарищ по-польски называется "mrowkozer". Mrowk=муравей, следовательно это муравьед:

![mrowkozer.jpg](part-2-animals/vertebrates/mammalia/mrowkozer.jpg)

А вот это - "mrowkojad". Тоже муравьед, получается:

![mrowkojad.jpg](part-2-animals/vertebrates/mammalia/mrowkojad.jpg)

Коала:

![koala.jpg](part-2-animals/vertebrates/mammalia/koala.jpg)

Летучая мышь:

![bat.jpg](part-2-animals/vertebrates/mammalia/bat.jpg)

Белка-летяга:

![polatucha.jpg](part-2-animals/vertebrates/mammalia/polatucha.jpg)

Дикобраз:

![dikobraz.jpg](part-2-animals/vertebrates/mammalia/dikobraz.jpg)

Кабанчик (по-польски "guziec"):

![kaban.jpg](part-2-animals/vertebrates/mammalia/kaban.jpg)

##### Обезьяны:

Мэгот:

![magot.jpg](part-2-animals/vertebrates/mammalia/monkeys/magot.jpg)

Носач:

![nosacz.jpg](part-2-animals/vertebrates/mammalia/monkeys/nosacz.jpg)

Шатанка:

![szatanka.jpg](part-2-animals/vertebrates/mammalia/monkeys/szatanka.jpg)

Кошкодан горный. Я так понимаю, "кошкоданом" прозвали за схожесть с кошкой:

![koszkodan.jpg](part-2-animals/vertebrates/mammalia/monkeys/koszkodan.jpg)

На этом буду заканчивать экспозицию второго зала "животные". Фоток ещё очень-очень много; я выбирал только самые интересные, а остальные нещадно отметал, чтобы не растягивать и без того длинное изложение. Следовательно, прошу не строить иллюзий, как будто представленные фотки - это прям всё, что было выставлено в том зале. На самом деле это процентов 20 от того, что было выставлено.

Итак, после посещения залы с животными мы пошли в третий зал "растения". В третьем зале осмотрели очень мало, потому что:
1. Было менее получаса до закрытия музея
2. Сотрудники музея предупредили нас, что у ихнего музея есть изюминка "зал скелетов", и посоветовали посетить именно его.
3. Экспонатов в зале с растениями было относительно немного - меньше сотни. В основном это гербарии, но был и, например, плод сейшельской пальмы в форме задницы:

![palma.jpg](part-3-plants/palma.jpg)

Итак,

## Часть 4 "Скелеты"

Змея:

![snake.jpg](part-4-skeletons/snake.jpg)

Крокодил нильский:

![krokodyl.jpg](part-4-skeletons/krokodyl.jpg)

Лягушка водная (Rana esculenta):

![zhaba-wodna.jpg](part-4-skeletons/zhaba-wodna.jpg)

Лягушка травяная (Rana temporia)

![zhaba-trawna.jpg](part-4-skeletons/zhaba-trawna.jpg)

Черепаха:

![turtle.jpg](part-4-skeletons/turtle.jpg)

Человек:

![homo-sapiens.jpg](part-4-skeletons/homo-sapiens.jpg)

Слон:

![elephant.jpg](part-4-skeletons/elephant.jpg)

Карп:

![karp.jpg](part-4-skeletons/karp.jpg)

Сом:

![som.jpg](part-4-skeletons/som.jpg)

Скат:

![skat.jpg](part-4-skeletons/skat.jpg)

Также были скелеты носорога, гиббона, лося, зебры.

Отдельно хочу сказать спасибо музею за образовательные плакаты.

На этом плакате рассказывается про структуру кости и отдельно указывается, что в Эйфелевой башне структура похожая на структуру кости:

![1.jpg](part-4-skeletons/plakats/1.jpg)

Следующий плакат называется "Изменение функций гомологичных элементов черепа". Череп по польски будет "czaszka = чашка".

![2.jpg](part-4-skeletons/plakats/2.jpg)

Гомология костей черепа:

![4.jpg](part-4-skeletons/plakats/4.jpg)

Разновидности хребцов:

![3.jpg](part-4-skeletons/plakats/3.jpg)

Итак, посмотреть скелеты мы успели, поэтому немножечко и "похапцем" оглядели также

## Часть 3 "растения"

![1.jpg](part-3-plants/1.jpg)

![2.jpg](part-3-plants/2.jpg)

![3.jpg](part-3-plants/3.jpg)

И напоследок - немного юмора:

![skelety-wc.jpg](skelety-wc.jpg)

Итого - крайне, вот прямо КРАЙНЕ рекомендую музей.