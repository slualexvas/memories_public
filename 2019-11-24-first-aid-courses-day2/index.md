 Курсы первой помощи -- день 2 
===============================

Очень устал, поэтому напишу кратко:

Разобранные темы:
-----------------

**1. Помощь при травмах** (кровотечения, повязки, обработка ран, обработка переломов...). В частности, важный факт: если у человека перелом, и если ты вызвал скорую, то иммобилизацию делать не надо (т.е. никаких шин, никаких палок к человеческой конечности привязывать не надо). Причина — в скорой шины лучше, и наложат они их лучше. Мы учились делать повязки для остановки кровотечения и накладывать жгут (разные виды жгутов).

Ещё один важный факт: йод, зелёнка, спирт — нежелательные антисептики, потому что обжигают кожу и, как следствие, ухудшают регенерацию. Лучший вариант — хлоргексидин. Наихудший вариант — перекись.

**2. Помощь при ожогах, обморожениях.**

Солнечные ожоги — ничем кожу не мазать. Если печёт — прохладная вода. Если больно печёт — спрей на водной основе с анестезирующим эффектом. Пантенол и любые средства на жировой основе — нельзя. Сметана или кефир — совсем плохо.

При термических/химических ожогах: пузыри не прокалывать и стараться, чтобы они подольше не лопались. Сразу после ожога сунуть руку под холодную воду, чтобы охладиться. Мазать жиром (в т.ч. кремы/мази/кефир/Пантенол) — нельзя.

При обморожении — 1) постепенно отогревать; 2) очень аккуратно раздевать; 3) не оставлять без присмотра.

**3. Помощь при отравлениях.**

Промывать живот до чистой воды (процесс — выпить воду сколько сможешь и вырвать в раковину. Если вырвал не чистой водой — повторить процесс). Диарее — не мешать, много пить. Если за день-два диарея не прошла — обращаться к врачу. Если критичная температура — обращаться к врачу раньше.

Марганцовка — строжайше запрещена: если, не дай боже, будет слишком сильный раствор — ожоги пищевода/желудка обеспечены. Если в растворе будет кристаллик — получим прободную язву. Пока марганцовку не запретили на государственном уровне — многие педиатры жаловались на неё.

Кстати, ещё педиатры жаловались на ожоги верхних дыхательных путей, которые возникали в результате детского «дыхания паром над картошкой».

Итоги:
------

Ребята — шикарнющие.

Курсы — шикарнющие.

Рекомендую — всем.

P.S.
----

А у меня теперь сертификат есть)