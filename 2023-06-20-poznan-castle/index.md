# Музей "Императорский замок" в Познани.

Мы приехали в Познань и пошли пешком в музей, который называется "Императорский замок" (Cesarski Zamek). В музее есть два тарифных плана:
- Ходишь с аудиогидом = 15 злотых с человека
- Ходишь с бумажкой = вроде бы 7 злотых с человека

Мы с Кариночкой взяли по аудиогиду, потому что у аудиогида есть украинский язык. Аудиогид здесь менее навороченный, чем в музее Чарторыйских: там он сам "понимал", к какому экспонату ты подошёл, и сам включал соответствующий разговор. А вот здесь нужно было делать так:

1. Находишь в музее табличку с номером (1, 2, 3, ..., 26)
2. Нажимаешь на гиде номер таблички (например, "25" и кнопку "Play")
3. Гид тебе рассказывает всё про табличку 25
4. После этого гид тебе говорит: "а чтобы пройти к следующей табличке, нужно повернуть направо, пройти по коридору и подняться по лестнице"

## 13:08 - Табличка 1 из 26: вход.

![01.jpg](01.jpg)

Здесь нам рассказали общие вещи: что раньше это был замок, а сейчас его отдали Познанскому отделению культуры, и он функционирует как культурный центр: в нём проводятся выставки, выступления артистов, и тд.

После этого гид сказал, мол, "идите туда-то к следующей табличке".

## 13:09 - Табличка 2 из 26: зал при гранитных ступеньках

На фотке ничего интересного, поэтому её показывать не буду. Гранитные ступеньки тоже показывать не буду, потому что для них есть аж две таблички попозже.

## 13:18 - Табличка 3 из 26: аммонитовый коридор

![03-ammonite-corridor.jpg](03-ammonite-corridor.jpg)

Справа видно окна, которые ведут во двор замка. Сам этот замок был построен в начале 1900-х годов немецким императором Вильгельмом (тогда Польши как государства не было, а Познань была под оккупацией Германии).

Цель замка - не оборонная, а представительская: чтобы было где остановиться, чтобы было чем похвастаться перед иностранными монархами, и тд.

В 1910-м году, когда замок достроили, то император Вильгельм приехал, и устроили пышные празднества. Поляки, жившие в Познани, позакрывали ставни окон, чтобы показать, что они всем этим празднествам не рады.

Во дворе замка (на который выходят окна справа) были благотворительные концерты по случаю празднования.

Слева - ряд дверей. Двери вроде бы построены тоже Вильгельмом, но налепки над дверями (такая налепка называется "картУш") были сделаны нацистами. На картУшах изображены растительные символы: листья дуба, яблоки, колоски пшеницы, и тд. Эти символы использовались нацистами (читать 3 пункта с конца по этой ссылке: http://bouriac.ru/ANS/ANS0050.htm

Кстати, у Штирлица на петлицах тоже был дубовый лист.

![03-1-ammonite.jpg](03-1-ammonite.jpg)

А эта фотка объясняет, почему коридор назывался "аммонитовый": потому что во мраморе, которым устелен пол коридора, можно найти контуры окаменелых улиток. Такая улитка называлась "аммонит": https://ru.wikipedia.org/wiki/Аммониты_(головоногие)

## 13:35 - Табличка 4 из 26: мозаичный зал

![04-mosaic-hall.jpg](04-mosaic-hall.jpg)

Зал называется "мозаичным", потому что на потолке нарисована мозаика с символами солнца, звёзд и цветов. Эти символы тоже использовались нацистами.

Этот зал был повреждён при освобождении Познани от нацистов советской армией.

Это видно, например, в дырках от пуль и осколков на переднем плане в правой части фотки.

Также это можно почувствовать, если потрогать рукой квадратную тумбу, которая расположена в нижнем конце перил: левая тумба холодненькая, а правая тумба теплее. Это потому, что левая тумба мраморная, а правую мраморную тумбу раздолбали в ходе боевых действий, и потому сделали подделку из штукатурки.

## 13:41 - Табличка 5 из 26: вход до проходной залы

![05.jpg](05.jpg)

Справа от лестницы видим барельефы, на которых изображено два голых мужика. Один мужик (слева на фотке) с книжкой, а второй - с молотком и мечом. Эти мужики символизируют идеал гражданина Третьего Рейха: либо учёный, либо рабочий.

## 13:51 - Табличка 6 из 26: проходная (автомобильная) зала.

![06-car-hall.jpg](06-car-hall.jpg)

![06-car-hall-with-throne.jpg](06-car-hall-with-throne.jpg)

Посреди залы есть полоса в земле. Эта полоса предназначена для автомобиля, который должен забирать императора с ближайшего вокзала, довозить к этой зале, заезжать в неё через двери с улицы, высаживать императора и выезжать через противоположные двери. После этого император по замку шёл в свои жилые покои и там отдыхал.

Белого трона здесь не было: он стоял в другой комнате, но та комната не сохранилась, и потому трон переместили сюда. Но сам трон принадлежит Вильгельму. 

Что интересно, трон сделан как двойной, но на самом деле в нём Вильгельм сидел один. Второе место предназначено для Бога: вроде как Вильгельм с Богом вместе правят.

По углам стоят 4 огромные вазы. На вазах изображены 4 занятия:
- Сбор пшеницы
- Сбор винограда
- Охота
- Не помню что

Эти вазы, как вы можете догадаться, от нацистов.

## 14:07 - Табличка 7 из 26: сразу после подъёма на первый (по-нашему это второй) этаж

Фотка неинтересная, поэтому я её не добавлял.

### 14:18 - Табличка 8 из 26: зал с камином

![08-fireplace-hall.jpg](08-fireplace-hall.jpg)

Эта зала находится справа от входа на этаж. Это был кабинет премьер-министра. Сначала немецкого, потом польского. 

Раньше над этим залом была башня, но в 1945-м башня была сильно повреждена, поэтому её убрали.

![08-balcony.jpg](08-balcony.jpg)

Также в зале с камином есть выход на балкон.

Балкон пристроили по желанию Гитлера - он собирался толкать речи перед толпой. Для такого дела на балконе даже сделали обогрев в полу - на случай, если речь будет длинная, а погода холодная. А Гитлер, подлец, так и не приехал ни разу в этот замок.

После 1945 года Польша оказалась под оккупацией СССР, и в этом кабинете был сотрудник администрации СССР. В июне 1956-го года он увидел многотысячную толпу, которая протестовала. Именно это стало началом первого антисоветского восстания в Польше https://ru.wikipedia.org/wiki/Познанский_июнь.

## 14:07 - Табличка 9 из 26: лифт возле жилых комнат (слева от входа на этаж)

![09-lift.jpg](09-lift.jpg)

Особенность лифта: механизм не на крыше, а в подвале. Чтобы быть менее уязвимым при артобстрелах и бомбардировках. Лифт построен ещё для Вильгельма, потому что тот хотел все технические новинки: водопровод, электричество и тд.

### 14:26 - Табличка 10 из 26: Ясеневая комната

![10-ash-room.jpg](10-ash-room.jpg)

Спальня первых лиц государства. Здесь спал Вильгельм, потом польские президенты, потом она предназначалась для Гитлера.

В нацистские времена комнату украсили методом инкрустации (наклеивания на дерево другой породы дерева):

- Напротив входа (видно на фото) сделали две картинки:
  - Слева над дверью картинка с символами войны (шлем, меч и тд)
  - Справа над дверью с символами уборки хлеба: серп, сноп и тд
- Над входом (не видно на фото) нарисованы символы ночи: сова и майские жуки. И дуб.

## 14:33 - Табличка 11 из 26: императорская мебель.

![11-furniture.jpg](11-furniture.jpg)

Это красивые сундуки, в которых ничего не лежало, ведь они использовались как украшение.

Император тратил на путешествия много времени (чуть ли не больше, чем на управление), и потому заказывал новые сундуки в стиле тех мест, где побывал.

## 14:35 - Табличка 12 из 26: императорский коридор

Фотка - см. выше: тот же коридор, где стоит мебель.

## 14:39 - Табличка 13 из 26: Ореховая комната

![13-wallnut-room.jpg](13-wallnut-room.jpg)

Место отдыха для первых лиц.

При Вильгельме эту комнату называли "Норвежская", и она была украшена шкурами животных, и даже в ней то ли поставили, то ли собирались поставить небольшую викинговскую ладью.

Вильгельм любил в этой комнате посидеть, покурить табак и попить пива.

---

![13-birch-room.jpg](13-birch-room.jpg)

А это берёзовая комната.

---

![13-marble-room.jpg](13-marble-room.jpg)

А это мраморная комната

## 14:44 - Табличка 14 из 26: Колонный зал

![14-column-hall.jpg](14-column-hall.jpg)

В этом зале была куча народу, которая что-то там снимала.

Ещё в этом зале есть барельефы из истории Польши, которые добавлены сюда в 1960-х годах. Кстати, слово "барельеф" переводится на английский язык как bas-relief, а на польский - как płaskorzeźba.

Вот эти барельефы: https://www.youtube.com/watch?v=mMT7jpd9uNc

## 14:47 - Табличка 15 из 26: фотографии Стефана Улатовского в 1929 году.

Фотографии являются ценным источником знаний про замок (какой балкон был, какого окна не было, и тд).

Я решил не фоткать эти фотографии со стены, потому что вам, дорогие читатели, будет приятнее посмотреть их из интернета: https://mediateka.ckzamek.pl/object/fotografie-romana-stefana-ulatowskiego/

Кстати, попутно я выяснил, что в Познани есть коллекция фотографий (и Улатовского, и других фотографов), которая называется CYRYL: https://cyryl.poznan.pl

## 14:51 - Табличка 16 из 26: кабинет императрицы.

![16-cabinet-emperor-wife.jpg](16-cabinet-emperor-wife.jpg)

В углу трон. Большие застеклённые двери на балкон, чтобы в комнате было светло и было видно сад.

Императрица занималась благотворительностью, но поляки её всё равно не любили.

## Табличка 17 из 26: балкон

![17-balcony.jpg](17-balcony.jpg)

Под балконом находится сад:

![17-garden.jpg](17-garden.jpg)

А вот так выглядит вид из правого угла балкона:

![17-balcony-right-corner.jpg](17-balcony-right-corner.jpg)

## 14:59 - Табличка 18 из 26: гранитные ступеньки

В самом начале экскурсии, при рассмотрении таблички №2, мы находились в "зале перед гранитными ступеньками". 

Сейчас мы вернулись к этому залу и зашли к гранитным ступенькам:

![18-granite-stairs.jpg](18-granite-stairs.jpg)

Высотой они метров 5. Были они построены уже при нацистах, и вели на второй этаж (а по-польски - первый) к резиденции Артура Грейзера - нацистского наместника в Польше.

Приглашаю также обратить внимание на стрёмный орнамент перил: какие-то кресты и круги. Аудиогид говорил, что орнамент специально сделан таким стрёмным, чтобы устрашать посетителей наместника.

## 15:01 - Табличка 19 из 26: гранитные ступеньки, вид сверху.

Не помню ни интересных фоток, ни интересного текста на этом месте.

## 15:03 - Табличка 20 из 26: кессонный зал.

![20-kesson-hall.jpg](20-kesson-hall.jpg)

Кессон (от французского caisson - ящик или от итальянского cassetta - коробка) в архитектуре - ячейка, на которые разделён потолок.

На потолке видно ячейки - это и есть кессоны.

В этом зале есть несколько экспонатов и документов, которые касаются замка. Аудиогид отдельно уточнил, что замок не является музеем, поэтому не имеет права коллекционировать экспонаты и документы. Странное заявление, на мой взгляд.

Среди экспонатов есть стулья:

![20-chairs.jpg](20-chairs.jpg)

Слева направо стулья:
- Императора (красивый стул, обитый кожей)
- Гитлера (менее роскошный стул, и обитый не кожей, а красной тканью)
- Сотрудника ратуши (стул попроще, но зато поновее)
- Зала культурного центра (хоть и самый новый стул, но самый гаденький. А почему? А потому что предназначен не для чиновников, а для простых людей)

Из этого зала есть лестница наверх. Поднявшись по лестнице, видим деревянный ящик на колёсах:

![20-scarbiec-dla-dzieci-przyslosci.jpg](20-scarbiec-dla-dzieci-przyslosci.jpg)

На ящике написано "scarbiec dla dzieci przyslosci", что переводится "сокровище для детей будущего".

Смысл этой штуки я ни тогда не понял, ни потом не нагуглил.

## Табличка 21 из 26: 

Где-то я её пропустил.

## 15:16 - Табличка 22 из 26: южный коридор

![22-south-corridor.jpg](22-south-corridor.jpg)

Это коридор, который ведёт к резиденции Грейзера. Аудиогид говорит, что коридор сделан максимально однообразным, чтобы опять же нервировать посетителей. Для этой же цели окна сделаны матовыми, чтобы природа и небо не успокаивали людей.

## 15:19 - Табличка 23 из 26: медиатека

![23-przedpokoj.jpg](23-przedpokoj.jpg)

Сейчас это место называется медиатекой. Тут стоят компьютеры, и любой желающий может за компьютер сесть и полистать старинные фотки:
- Разных частей замки
- Императорской семьи
- Замкового управляющего и его родственников

А вот при нацистах здесь было место, где посетители сидели и ожидали, пока Грейзер соизволит их принять. В левом краю фотки видно большой деревянный шкаф. Это шкаф для верхней одежды посетителей.

## 15:25 - Табличка 24 из 26: кабинет Грейзера

![24-cabinet-of-arthur-greiser.jpg](24-cabinet-of-arthur-greiser.jpg)

Вот кабинет Грейзера. В нём есть три потайных двери (на фотке видно две). За одной из дверей - путь для эвакуации. За второй - сейф. За третьей - не помню, но вроде бы вход для слуг.

## 15:29 - Табличка 25 из 26: травертиновый зал

![25-travertin-hall.jpg](25-travertin-hall.jpg)

Зал так назван по имени минерала "травертин", которым он облицован. https://ru.wikipedia.org/wiki/Травертин

Судя по цитате из Википедии "Также выделяется из подземных вод в пещерах, образуя сталактиты и сталагмиты", я делаю вывод: если кто играл в Minecraft, то он может вспомнить материал "капельник" из той игры: https://minecraft.fandom.com/ru/wiki/Капельник

После травертинового зала мы пошли в то крыло замка, где раньше был тронный зал, а сейчас там всё переделали под нужды культурного центра.

Итак, вот переделанная верхняя часть тронного зала (сейчас там кафе):

![25-cafe.jpg](25-cafe.jpg)

## 15:37 - Табличка 26 из 26: нижняя часть тронного зала

![26-throne-hall.jpg](26-throne-hall.jpg)

Именно здесь стоял раньше трон императора, который сейчас находится в проходной зале, возле железной дороги.

## Итог

Вот и закончилась экскурсия по Познанскому императорскому замку. Она была очень длинной и очень насыщенной. Про многие вещи я узнал впервые:
- Что император Вильгельм любил путешествовать
- Что Гитлер так и не побывал в Познанском замке, хотя для него построили балкон с подогревом
- Что первое антикоммунистическое восстание было в Познани, в июне 1956

Мне весьма понравилась идея "не делать из замка музей, а использовать его как культурный центр". Вышло весьма неплохо.

Единственный недостаток - я так и не смог найти в интернете все барельефы из колонного зала. Думаю, посмотреть их в спокойной обстановке было бы весьма познавательно.