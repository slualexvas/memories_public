# Музей янтаря в Гданьске

## Цена билета

Мы так удачно попали, что в этот день был день открытых дверей. Поэтому билеты нам попались бесплатно

## Интерьер музея

Стены музея чёрные, а освещение - той же цветовой гаммы, что и янтарь. Также в музее есть несколько искусственных (не янтарных, а стеклянных) деталей интерьера, которые добавляют визуальной красоты.

Первая такая деталь - это колонны из жёлто-оранжевого стекла с лампочками внутри:

![01-glass-columns.jpg](01-glass-columns.jpg)

Вторая такая деталь - это лампочки, встроенные в стену, и закрытые панелями из такого же жёлто-оранжевого стекла:

![02-glass-panels-in-walls.jpg](02-glass-panels-in-walls.jpg)

Сам музей находится на двух этажах здания - втором и третьем:
- На втором этаже экспозиция посвящена янтарю в природе
- На третьем этаже экспозиция посвящена янтарю в культуре (например, все рукотворные изделия из янтаря находятся на третьем этаже).

На первом этаже находятся кассы, туалет, а также магазин янтаря.

## Этаж 2: янтарь в природе

Сначала были показаны много кусочков янтаря, чтобы мы увидели, что это вообще такое.

![03-amber-pieces.jpg](03-amber-pieces.jpg)

Потом был показан видосик, где объяснено происхождение янтаря:
- Есть хвойные деревья
- У этих деревьев есть смола
- У некоторых деревьев есть пустоты (они называются "карманы") под корой
- Эти пустоты заливаются смолой
- Может так оказаться, что дерево после смерти упадёт в морскую воду
- Тогда дерево сгниёт, а кусок смолы будет долго-долго лежать в воде, то он напитается морскими минералами и станет янтарём.

После этого была парочка стендов, посвящённых объектам, которые застревают в янтаре (точнее, в смоле, которая становится янтарём позднее). 

Первый стенд посвящён растительным объектам, например:

![04-leaf-in-amber.jpg](04-leaf-in-amber.jpg)

Второй стенд посвящён животным объектам: 

![05-insects-in-amber.jpg](05-insects-in-amber.jpg)

Животные объекты варьируются от насекомых и аж до небольших ящериц.

Чтобы оценить размеры янтаря, скажу так: самый большой кусок, который я видел, был в диаметре чуть меньше полуметра:

![06-big-piece.jpg](06-big-piece.jpg)

Завершает этаж вот такая симпатичная фотозона моря:

![07-sea-photozone.jpg](07-sea-photozone.jpg)

## Третий этаж - янтарь в культуре

Сборщик янтаря низкоуровневый (т.е. ходящий по берегу и разгребающий песок):

![08-amber-searcher.jpg](08-amber-searcher.jpg)

Сборщик янтаря высокоуровневый (т.е. лазящий под водой в водолазном костюме). Это образец костюма из 1864 года:

![09-underwater-costume.jpg](09-underwater-costume.jpg)

Инструменты для обработки янтаря:

![10-amber-tools.jpg](10-amber-tools.jpg)

Станок для обработки янтаря:

![11-amber-machine.jpg](11-amber-machine.jpg)

Мастерская средневековая:

![12-workshop-medieval.jpg](12-workshop-medieval.jpg)

Мастерская современная:

![13-workshop-modern.jpg](13-workshop-modern.jpg)

Карта торговых путей с янтарём:

![14-trade-map.jpg](14-trade-map.jpg)

Видосики, как обрабатывали янтарь:

- Пилили ниткой, примерно как сливочное масло: https://youtu.be/velWU9427Iw
- Сверлили (а палку с ниткой использовали, чтобы быстрее крутить сверло): https://youtu.be/ieVuWvblz0A

Ну и наконец то, чего читатель так долго ждал: изделия из янтаря! Этих самых изделий было больше сотни, но чтобы не утомлять читателя и меня - выложу лишь несколько:

![15-amber-madonna.jpg](15-amber-madonna.jpg)

![16-wood-and-grail.jpg](16-wood-and-grail.jpg)

![17-clock.jpg](17-clock.jpg)

![18-necklace.jpg](18-necklace.jpg)

## Этаж 1: магазин янтарных изделий

Бусы (которые справа) = 2500 злотых:

![19-shop-necklace.jpg](19-shop-necklace.jpg)

Кулоны
- от 4 до 8 граммов янтаря
- оправа = золото или серебро
- цена = от 500 до 2000 злотых:

![20-shop-kulons.jpg](20-shop-kulons.jpg)

Лампы с абажуром из янтаря (я не увидел цену):

![21-amber-lamps.jpg](21-amber-lamps.jpg)

Крашеный янтарь:

![22-shop-painted-amber.jpg](22-shop-painted-amber.jpg)

Просто красивые бусы:

![23-shop-necklace.jpg](23-shop-necklace.jpg)

Интересная технология для увелирки: делаем золотую клетку, а в неё "сажаем в заточение" кусочек янтаря:

![24-gold-cage.jpg](24-gold-cage.jpg)

Ну и вот вам ещё кулоны (от 400 до 700 злотых):

![25-shop-kulons.jpg](25-shop-kulons.jpg)

## Итоги:

Музей - симпатичный.

Посетить - советую.

Время для посещения - от 1 до 1.5 часа.

P.S. А цены в магазине - сильно завышенные. Мы потом купили Кариночке янтарный кулон (чуть поскромнее, чем на последней фотке) за 100 злотых в одном из магазинчиков возле канала. Вот фото Кариночкиного кулона:

![26-karina-kulon.jpg](26-karina-kulon.jpg)

P.P.S. Украинское и польское название янтаря (бурштин) происходит от немецкого названия янтаря (bernstein):
- stein переводится с немецкого как "камень"
- bern с современного немецкого никак не переводится, но происходит от древнегерманского слова "börnen", что означает "гореть". Таким образом, "Bernstein" дословно можно перевести как "горящий камень" (а в современном немецком языке "гореть" - это brennen). Английское "burn" происходит от того же источника.
