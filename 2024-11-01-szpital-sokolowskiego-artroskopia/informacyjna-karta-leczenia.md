# (здесь мои имя, фамилия, PESEL)

## Specjalistyczny Szpital im. dra Alfreda Sokołowskiego

**Oddział Chirurgii Urazowo-Ortopedycznej**  
58-309 Wałbrzych, ul. Sokołowskiego 4  
Kody resortowe: I - 000000001389; V - 01; VII - 010; VIII - 4580  
NIP: 8862385315; REGON: 890047446

## Karta informacyjna leczenia szpitalnego

**Nr Księgi Gł.:** 50704/2024  
(мои данные: адрес, телефон и тд)

Przyjęty(a) do szpitala dnia: **2024-11-11** o godz. **08:16**.  
Przyjęty na oddział dnia: **2024-11-11** o godz. **08:16**  
Przebywał(a) od **2024-11-11** o godz. **08:16** do **2024-11-13** o godz. **11:00** w Oddziale Chirurgii Urazowo-Ortopedycznej.  
Data zakończenia pobytu: **2024-11-13** o godz. **11:00**

---

## Rozpoznania

**M23.3** - Uszkodzenie rogu tylnego łąkotki przyśrodkowej kolana prawego

## Leczenie

**Oddział Chirurgii Urazowo-Ortopedycznej**  
12/11/2024 operacyjne (lek. Adam Krzywda): artroskopia stawu kolanowego prawego, szycie rogu tylnego MM (Stryker Air+)  
Użyte implanty NIE stanowią przeciwwskazania do wykonania rezonansu magnetycznego dowolnej okolicy ciała w przyszłości (MRI<3T)

## Epikryza

36-letni pacjent przyjęty do Oddziału celem artroskopii stawu kolanowego prawego. Zakwalifikowany do zabiegu, który odbył się 12/11/2024, przebieg gładki. Podczas obserwacji w Oddziale stan pozostawał dobry. Wypisany w stanie ogólnym dobrym z zaleceniami.

## Zalecenia

**Oddział Chirurgii Urazowo-Ortopedycznej**
- Odciążenie operowanej kończyny przez 6 tygodni, kule łokciowe, orteza na kolanowy z regulacją kąta zgięcia zgodnie z protokołem:
    - do 2 tyg w zgięciu 30 st na sztywno, 2-4 tydz 30-60 st, 4-6 tydz 0-90 st
    - unikanie zginania kolana powyżej 120 st przez 10 tyg, unikanie kucania przez 6 miesięcy od zabiegu

### Higiena rany pooperacyjnej
Zmiana opatrunku codziennie, dezynfekcja rany Octeniseptem. Usunięcie szwów skórnych na około 14 dobę od zabiegu – prosimy o wykonanie przez Lekarza Rodzinnego lub Pielęgniarkę.

### Dalsze leczenie
Kolejne wizyty pod kontrolą Poradni Ortopedycznej za 6-8 tygodni. Skierowanie wydano, należy się pilnie zarejestrować.

### Leki
- **NEOPARIN** [INJ. 0,04 G/0,4 ML = 4000 J.M., 3 opak., po 10 AMPUŁKOSTRZYKAWEK, 1x1 sc, R]
- **SKUDEXA** [TABL. POWL., 1 opak., po 30 TABL., 2x1 po, 100%]
- **eRecepta:** 5772
- **eSkierowanie:** 3914 - Poradnia chirurgii urazowo-ortopedycznej
