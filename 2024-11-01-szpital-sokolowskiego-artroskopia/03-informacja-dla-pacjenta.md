# Informacja dla Pacjenta

Specjalistyczny Szpital im. dra Alfreda Sokołowskiego

## Formularz Świadomej Zgody na zabieg diagnostyczny lub terapeutyczny stwarzający podwyższone ryzyko dla Pacjenta**

## ZABIEG ARTROSKOPII**

IMIĘ I NAZWISKO PACJENTA: ...
PESEL/data urodzenia: ...

## 1. RODZAJ SCHORZENIA, WSKAZANIA I PRZYGOTOWANIE DO ZABIEGU

Szanowna Pani, Szanowny Panie,

W wyniku badania fizykalnego i badań dodatkowych stwierdzono u Pani/Pana schorzenie wymagające wykonania artroskopii.

Artroskopia to zabieg polegający na „zajrzeniu” do wnętrza stawu z dwóch celów: w celu badania i leczenia posiadanych przez Panią/Pana zmian. Najczęściej wykonuje się artroskopię kolana, ale zabieg ten można badać także inne stawy, np. barkowy, biodrowy, skokowy, nadgarstkowy. Artroskopię można także wykonywać przy różnych stanach chorobowych, takich jak uszkodzenia łąkotek, więzadeł, ciał obcych, chrząstek, zmian zapalnych, zrostów, etc. Przed przystąpieniem do operacji lekarz prowadzący będzie rozmawiać z Panią/Panem na temat przebiegu operacji, przygotowania do operacji, wyników badań, rokowania oraz postępowania przed i po zabiegu.

Przed planowaną operacją - zgodnie z wytycznymi lekarza prowadzącego - należy odpowiednio się przygotować, w tym m.in. zaprzestać przyjmowania niektórych leków, np. rozrzedzających krew, a także, jeśli to wymagane, wykonać dodatkowe badania krwi.

## 2. PRZEBIEG OPERACJI

Po zakwalifikowaniu do operacji konieczne jest przeprowadzenie podstawowych badań.

Zabieg operacyjny może być przeprowadzony w znieczuleniu miejscowym, przewodowym lub ogólnym.

W czasie trwania zabiegu stosuje się opaski uciskowe na udo ograniczające dopływ krwi do miejsca operacyjnego. Po niewielkim nacięciu do stawu wprowadza się artroskop (wziernik). Staw wypełnia się płynem, co umożliwia dokładne obejrzenie wnętrza stawu oraz przeprowadzenie niezbędnych procedur leczniczych. W tym celu wykonuje się dodatkowe nacięcie (wyjątkowo dwa nacięcia), przez które wprowadza się narzędzia chirurgiczne. Wnętrze stawu monitoruje się na monitorze.

Po zabiegu w stawie umieszcza się dren, przez który odsysa się zbierającą się w nim krew. Rany powstałe zamyka się pojedynczymi szwami i zakłada opatrunek.

## 3. Inne dostępne metody leczenia

Inna dostępna metoda leczenia jest operacja tradycyjna, polegająca na otwarciu stawu kolanowego za pomocą dużego cięcia. Przy tego rodzaju zabiegu ryzyko infekcji jest znacznie większe, jak również wydłuża się proces gojenia i rehabilitacji.

## 4. MOŻLIWOŚĆ WYSTĄPIENIA POWIKŁAŃ

Pomimo dużego doświadczenia i staranności ze strony zespołu operacyjnego w czasie operacji i po niej może dojść do powikłań, które przeważnie są natychmiast rozpoznawane i leczone. Mogą to być:

- krwawienia wewnątrzstawowe,
- przedłużające się wysięki,
- zakażenie miejsca operowanego,
- ograniczenie ruchomości stawu,
- uszkodzenie nerwów,
- uszkodzenie chrząstki stawowej,
- odczyny na zastosowane implanty,
- zakrzepica żył,
- zatorowość płucna,
- inne najrzadziej występujące powikłania.

Usunięcie łąkotki może wiązać się z przyspieszeniem zmian zwyrodnieniowych stawu.

Ryzyko powikłań związanych z tego typu zabiegu w tut. oddziale nie przekracza 1%. Wymienione powikłania mogą pojawić się w przebiegu pooperacyjnym u każdego pacjenta, jednak ich ogólna częstotliwość występowania nie jest wysoka. Ich liczba zwiększa się u chorych na cukrzycę, u pacjentów po dwóch operacjach i osób otyłych.

## 5. OPIS PROGNOZY POOPERACYJNEJ I POWIKŁAŃ ODLEGŁYCH

Powrót do zdrowia po zabiegu artroskopowym następuje znacznie szybciej niż w przypadku tradycyjnej operacji. Pacjenci wracają do domu tego samego dnia. Nacięcia są zwykle bezbolesne, ale przez kilka dni można odczuwać obrzęk i dyskomfort w obszarze stawu. Pacjent może zacząć chodzić tego samego dnia po zabiegu (w przypadku kolana jest zalecane odciążenie stawu przez kilka dni). W przypadku innych stawów czas powrotu do pełnej aktywności zależy od rodzaju i przyczyn wykonywania, przez krótki czas można wymagać stosowania kul. Rana goi się w ciągu 7-10 dni, a po około 3 tygodniach można powrócić do pełnej aktywności. Powrót do pełnej sprawności, szczególnie w sporcie, możliwy jest po kilku miesiącach.

## 6. O CZYM POWINNA/POWINIEN PANI/PAN POWIEDZIEĆ LEKARZOWI:

Na ryzyko operacji mają wpływ stan ogólny chorego oraz choroby współistniejące oraz choroby przebyte. Aby w porę można było rozpoznać zagrożenia prosimy odpowiedzieć na następujące pytania:

- Czy wiadomo, że istnieją u Pani/Pana zaburzenia przemiany materii (np. cukrzyca) lub inne choroby tj. choroby serca, naczyń, wątroby, tarczycy, układu nerwowego? **Tak / Nie**
- Czy istnieją u Pani/Pana współistniejące choroby zakaźne np. zapalenie wątroby, AIDS, nosicielstwo bakteryjne (gardło, nos, przewód pokarmowy)? **Tak / Nie**
- Czy stwierdzono zostały u Pani/Pana uczulenia lub nadwrażliwość na leki, plastry, lateks, rośliny, środki spożywcze, zwierzęta? **Tak / Nie**
- Czy występowały kiedykolwiek u Pani/Pana napady drgawek? **Tak / Nie**
- Czy przy u Pani/Pana wcześniejszych operacjach lub skaleczeniach (np. leczenie zębów) doszło do wzmocnionego krwawienia? **Tak / Nie**
- Czy wcześniej, w przypadku powstania ran, dochodziło do ropienia, opóźnionego gojenia, ropni, przetok, zgrubienia blizny po zagojeniu? **Tak / Nie**
- Czy przyjmuje Pani/Pan leki tj. nasercowe, przeciwbólowe, hormonalne, hamujące krzepliwość krwi (np. Sintrom, Synkumar, Aspiryna, Acard)? **Tak / Nie**
- Czy obserwowano u Pani/Pana zakrzepy żylne lub zatorowość płucną? **Tak / Nie**
- Kobiety w wieku rozrodczym: Czy istnieje możliwość, że jest Pani w ciąży? **Tak / Nie**

## 7. Prosimy aby Pani/Pan zapytał/a nas o wszystko co chciałaby Pani/Pan wiedzieć w związku z planowanym zabiegiem. Chętnie odpowiemy na wszystkie Pani/Pana pytania.

## 8. OŚWIADCZENIE PACJENTA

W pełni zrozumiałam/em informacje zawarte w tym formularzu oraz przekazane mi podczas rozmowy z lekarzem. Zapewniono mi nieograniczone możliwości zadania pytań i na wszystkie udzielono mi odpowiedzi i wyjaśnień w sposób dla mnie zrozumiały i niebudzący żadnych wątpliwości. Po zapoznaniu się z treścią tego formularza i rozmowie wyjaśniającej z lekarzem/pielęgniarką (imię i nazwisko): Mikołaj Łapiecka.

Zobowiązuję się spełnić wszelkie moje wymagania, co do informacji na temat dających się przewidzieć następstw każdorazowo wykonywanego zabiegu albo do ewentualnych skutków jego zaniechania w przypadku mojego schorzenia. Znane są mi możliwe powikłania związane z tym zabiegiem.

Bez zastrzeżeń (lub z powyższymi zastrzeżeniami) wyrażam świadomą zgodę na przeprowadzenie u mnie ARTROSKOPII:

Data: 11.09.2024

pieczątka i podpis lekarza | czytelny podpis pacjenta

czytelny podpis uprawnionego opiekuna

## 9. Nie zgadzam się na przeprowadzenie u mnie ARTROSKOPII:

Zostałam/em w pełni poinformowana/y o możliwych negatywnych konsekwencjach takiej decyzji dla mojego zdrowia i życia.

Data

pieczątka i podpis lekarza | czytelny podpis pacjenta

czytelny podpis uprawnionego opiekuna

## 10. UWAGA:

- W przypadku osób do 16-go roku życia zgodę wyraża i podpisuje przedstawiciel ustawowy (rodzic) lub opiekun prawny.
- W przypadku osób w wieku od 16-go roku życia do 18-go roku życia zgodę wyraża i podpisuje przedstawiciel ustawowy (rodzic) lub opiekun prawny oraz pacjent.
